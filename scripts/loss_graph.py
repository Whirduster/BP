#
#   Author: Tomas Libal
#   Date:   27.3.2019
#
#   This program will show learning curve based on train data
#

import sys

from bokeh.io import output_file, show
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, HoverTool
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter


parser = ArgumentParser(description="This program will show learning curve based on train data.",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)

parser.add_argument("-w", help="Show only weights which can be divided by step number", action="store",
                    dest="weights_iteration", type=int, default=1)
parser.add_argument("-s", help="graph will start from this index", action="store", dest="start", type=int, default=0)
parser.add_argument("path", help="Path to txt file")

args = parser.parse_args()

graph_data = open(args.path, "r").read()
lines = graph_data.split('\n')

x = []
y1 = []
y2 = []

for line in lines:
    if len(line) > 1:
        iteration, loss, avg = line.split(' ')
        if int(iteration) % int(args.weights_iteration) == 0 and int(iteration) > args.start:
            x.append(int(iteration))
            y1.append(float(loss))
            y2.append(float(avg))

# output to static HTML file
output_file("/home/xlibal00/Documents/BP/loss_graph.html")

source = ColumnDataSource(data=dict(
    x=x,
    y=y1,
    loss_val=y1,
))

# create a new plot
p = figure(
    title="Loss function",
    active_scroll="wheel_zoom",
    x_axis_label='Iterations', y_axis_label='Loss',
    sizing_mode='stretch_both'
)

# save as vector image
p.output_backend = "svg"


# add some renderers
p.line(x, y1, legend="loss", color="red", line_width=1.5)
loss_dots = p.circle('x', 'y', legend="loss", fill_color="blue", line_color="blue", size=4, source=source)
p.line(x, y2, legend="average loss", color="deepskyblue", line_width=2)
p.square(x, y2, legend="average loss", fill_color="deepskyblue", line_color="deepskyblue", size=3)

p.add_tools(
    HoverTool(
        tooltips=[("iteration", "@x"), ("loss", "@loss_val")],
        mode="hline",
        renderers=[loss_dots]
    )
)

p.legend.click_policy = "hide"

# show the results
show(p)
