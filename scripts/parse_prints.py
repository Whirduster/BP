import sys
import os
import subprocess
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter


# Check what options was selected
def check_iou_prob(main_parser, args):
    # both is selected
    if args.iou and args.probability:
        return args.iou, args.probability
    elif args.iou:
        return args.iou
    elif args.probability:
        return args.probability
    else:
        main_parser.error('Nothing to print. Please add at least from -p (--prob) and -i (--iou)')
        exit(1)


# ROC options
def roc_options(sub_p):
    sub_p.add_argument("-v", "--validate", help="This will print only first threshold results",
                       action="store_true", dest="validate")
    sub_p.add_argument("-g", "--graph_output", help="Output directory for ROC graphs.\n"
                                                    "(Default: /home/xlibal00/Documents/BP/roc_graphs/)",
                       action="store", dest="graph_output", default="/home/xlibal00/Documents/BP/roc_graphs/")
    sub_p.add_argument("-s", "--show", help="Save and show ROC curve, otherwise it will be only saved",
                       action="store_true", dest="show", default=False)


# Run ROC script
def run_roc(args, input_file, output_file, iterate):
    if args.validate:
        if args.show:
            output = subprocess.check_output(["python", "../roc.py", "-i", input_file, "-o", output_file, "--validate",
                                              "--show", "--header", iterate]).strip()
        else:
            output = subprocess.check_output(["python", "../roc.py", "-i", input_file, "-o", output_file, "--validate",
                                              "--header", iterate]).strip()
    else:
        if args.show:
            output = subprocess.check_output(["python", "../roc.py", "-i", input_file, "-o", output_file, "--show",
                                              "--header", iterate]).strip()
        else:
            output = subprocess.check_output(["python", "../roc.py", "-i", input_file, "-o", output_file,
                                              "--header", iterate]).strip()
    # print ROC output
    print(output)


# run subprocesses
def run(args, iterate):
    with open(args.input_file, "r") as input_file:
        if args.subcommand == 'write_and_show_roc':
            out_dir = args.output_file
            for threshold in range(int(args.threshold), 101):
                if threshold == 0:
                    threshold = 0.1
                args.output_file = os.path.join(out_dir, os.path.splitext(os.path.basename(arguments.input_file))[0]
                                                + "_" + os.path.splitext(os.path.basename(args.weights_file))[0]
                                                + "_th_" + str(threshold / 100) + ".txt")
                print(args.output_file)
                print(threshold)
                print(str(threshold / 100))
                if os.path.exists(args.output_file):
                    if args.overwrite:
                        os.remove(args.output_file)
                    else:
                        print("File \"" + args.output_file + "\" cannot be overwritten")
                        exit(1)
                # open output file for writing results of validation
                with open(args.output_file, "a+") as output_file:
                    p = subprocess.Popen(["./darknet", "detector", "test", args.data_file, args.cfg_file,
                                          args.weights_file, "-thresh", str(threshold / 100), '-iou', '-probability'],
                                         stdin=input_file, stdout=output_file)
                    p.wait()

            # run ROC save/show
            # run_roc(args, args.output_file, args.graph_output, iterate)

        elif args.subcommand == "show_roc":
            # run ROC save/show
            run_roc(args, args.input_file, args.graph_output, iterate)
        elif args.subcommand == "print_results":
            if isinstance(check_iou_prob(subparser_list[2], args), tuple):
                p = subprocess.Popen(["./darknet", "detector", "test", args.data_file, args.cfg_file, args.weights_file,
                                      "-thresh", args.threshold, args.iou, args.probability], stdin=input_file)
            else:
                p = subprocess.Popen(["./darknet", "detector", "test", args.data_file, args.cfg_file, args.weights_file,
                                      "-thresh", args.threshold, check_iou_prob(parser, args)], stdin=input_file)
            p.wait()
        elif args.subcommand == "draw_results":
            p = subprocess.Popen(["./darknet", "detector", "test", args.data_file, args.cfg_file, args.weights_file,
                                  "-thresh", args.threshold], stdin=input_file)
            p.wait()


# # confirm that we can loop over all files in given directory
# def confirm_loop():
#     usr_input = ''
#     while usr_input not in ['Y', 'N', "yes", "no", "y", "n", "YES", "NO"]:
#         usr_input = raw_input("Do you really want to loop through all files? [Y/N]: ")
#         if usr_input in ["N", "no", "n", "NO"]:
#             exit(0)


parser = ArgumentParser(description="This program run validation of trained network.",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)

subparsers = parser.add_subparsers(dest="subcommand")
arg_help_list = [
    {"subcommand": "write_and_show_roc", "help": "Write results to output file and create ROC curve \n"
                                                 "(ROC curve will be saved in roc_graph dir with name based on "
                                                 "output_file)"},
    {"subcommand": "show_roc", "help": "Show ROC curve (ROC curve will be saved in roc_graph dir)"},
    {"subcommand": "print_results", "help": "Print validation results to stdout"},
    {"subcommand": "draw_results", "help": "Draw every image with expected and detected bounding boxes"}
]

subparser_list = []
for subparser in arg_help_list:
    par = subparsers.add_parser(subparser['subcommand'], help=subparser['help'], description=subparser['help'],
                                formatter_class=RawTextHelpFormatter)
    # show_roc options
    if subparser['subcommand'] == "show_roc":
        par.add_argument("-f", "--input", help="Input file with validation results\n -OR- \n"
                                               "Directory of files with validation results",
                         action="store", dest="input_file", required=True)
        roc_options(par)
    else:
        # write_and_show_roc, print_results and draw_results options
        par.add_argument("-d", "--data", help=".data file (darknet/cfg/data/)", action="store",
                         dest="data_file", required=True)
        par.add_argument("-c", "--cfg", help=".cfg file (darknet/cfg/cfg/)", action="store", dest="cfg_file",
                         required=True)
        par.add_argument("-f", "--input", help="Input file (file with paths to images)",
                         action="store", dest="input_file", required=True)
        par.add_argument("-t", "--thresh", help="Threshold (0-100) (default 0.5)", action="store", dest="threshold",
                         default="5")

        # --dir or -w
        group = par.add_mutually_exclusive_group(required=True)
        group.add_argument("-w", "--weights", help=".weights file (result of training).\n"
                                                   "If --dir, it is unnecessary, if not, it is necessary",
                           action="store", dest="weights_file")
        group.add_argument("--dir", help="Directory with .weights files", action="store", dest="dir")

    subparser_list.append(par)

# write_and_show_roc
subparser_list[0].add_argument("-o", "--output", help="Output file (file in which will be validation results saved), "
                                                      "\nIf --dir is used -o must be an output directory",
                               action="store", dest="output_file", required=True)
subparser_list[0].add_argument("--overwrite", help="If file exist, with this option will be overwrite",
                               action="store_true", dest="overwrite", default=False)
roc_options(subparser_list[0])

# print_results
subparser_list[2].add_argument("-i", "--iou", help="Print IoU results (choose at least one from -i and -p)",
                               action="store_const", const="-iou", default="")
subparser_list[2].add_argument("-p", "--prob", help="Print probability results (choose at least one from -i and -p)",
                               action="store_const", dest="probability", const="-probability", default="")

arguments = parser.parse_args()

if arguments.subcommand == "show_roc":
    if os.path.isdir(arguments.input_file):
        # confirm_loop()
        # backup of input_dir
        input_dir = arguments.input_file
        print_header = "Y"
        for filename in os.listdir(arguments.input_file):
            if filename.endswith(".txt"):
                arguments.input_file = os.path.join(input_dir, filename)
                run(arguments, print_header)
                print_header = "N"
            else:
                continue
    else:
        run(arguments, "Y")
elif arguments.dir is not None and os.path.isdir(arguments.dir):
    print("-w option will be ignored if is set.")
    # confirm_loop()
    # backup of output dir
    output_dir = ''
    print_header = "Y"
    if arguments.subcommand == 'write_and_show_roc':
        # if output file is set
        if arguments.output_file is not None:
            output_dir = arguments.output_file
            # if output is not a directory
            if not os.path.isdir(arguments.output_file):
                print("\n-o must be a directory\n")
                subparser_list[0].print_help()
                exit(1)
    for filename in os.listdir(arguments.dir):
        if filename.endswith(".weights"):
            arguments.weights_file = os.path.join(arguments.dir, filename)
            if arguments.subcommand == 'write_and_show_roc' and output_dir != '':
                # create output file name
                arguments.output_file = os.path.join(output_dir,
                                                     os.path.splitext(os.path.basename(arguments.input_file))[0] + "_" +
                                                     os.path.splitext(filename)[0] + ".txt")
            run(arguments, print_header)
            print_header = "N"
        else:
            continue
else:
    # no iterations -> -w is necessary
    if arguments.weights_file is None:
        print("Please add -w argument")
        exit(1)
    run(arguments, "Y")
