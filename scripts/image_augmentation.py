#
#   Author: Tomas Libal
#   Date:   4.4.2019
#
#   This script will augment images in given directory and create new augmented dataset
#

import imageio
import os
from random import randint

import imgaug as ia
from imgaug import augmenters as iaa
from PIL import Image
from scipy import misc
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

parser = ArgumentParser(description="This program will aughment images in given directory",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)

parser.add_argument("-o", help="Output dir for images", action="store", dest="output", required=True)
parser.add_argument("-f", help="Path to file in witch will be augmented images paths saved", action="store",
                    dest="out_file", required=True)
parser.add_argument("-i", help="Input file with paths to images", action="store", dest="input", required=True)
parser.add_argument("-r", help="Root dir (all subdirs from this will be created in output dir)", action="store",
                    dest="root", required=True)
parser.add_argument("--show", help="Show each image before and after augmentation", action="store_true", dest="show")

mex = parser.add_mutually_exclusive_group()
mex.add_argument("--low", help="Low augmentation",
                 action="store_true", dest="low", default=False)
mex.add_argument("--medium", help="Medium augmentation",
                 action="store_true", dest="medium", default=False)
mex.add_argument("--high", help="High augmentation",
                 action="store_true", dest="high", default=False)

args = parser.parse_args()

ia.seed(1)  # i do not know what is this for, but it is necessary

# line counter in file
with open(args.input) as file:
    num_lines = sum(1 for line in file)

with open(args.out_file, "w+") as paths:
    # list of images to augmentation
    images = []
    for image_path, i in zip(open(args.input, "r").readlines(),
                             range(1, num_lines + 1)):
        image_path = image_path[:-1]  # erase linebreak
        print("Augmenting image " + str(i) + " of " + str(num_lines) + ": " + image_path)

        # create path to corresponding text path
        txt_path = os.path.splitext(image_path)[0] + ".txt"

        # get image size
        im = Image.open(image_path)
        im_width, im_height = im.size

        # list of bboxes
        bboxes = []

        # read original bboxes
        for line in open(txt_path, "r").readlines():
            coords = line.split()
            # delete first number -> useless
            del coords[0]

            # calculate coordinates (YOLO to pixel numbers)
            left = int((float(coords[0]) - float(coords[2]) / 2.0) * im_width)
            right = int((float(coords[0]) + float(coords[2]) / 2.0) * im_width)
            top = int((float(coords[1]) - float(coords[3]) / 2.0) * im_height)
            bottom = int((float(coords[1]) + float(coords[3]) / 2.0) * im_height)

            # bbox is outside of image
            if left < 0:
                left = 0
            if right > im_width - 1:
                right = im_width - 1
            if top < 0:
                top = 0
            if bottom > im_height - 1:
                bottom = im_height - 1
            # append to list of bboxes
            bboxes.append(ia.BoundingBox(x1=left, x2=right, y1=top, y2=bottom))

        image = imageio.imread(image_path)
        bbs = ia.BoundingBoxesOnImage(bboxes, shape=image.shape)

        # show original
        if args.show:
            ia.imshow(bbs.draw_on_image(image, thickness=2))

        if args.low:
            # select random number of transformations
            aug = iaa.Sequential([
                # Apply affine transformations to each image.
                # Scale/zoom them, translate/move them, rotate them and shear them.
                iaa.SomeOf(randint(1, 4), [
                    iaa.Affine(scale={"x": (0.85, 1.15), "y": (0.85, 1.15)}),
                    iaa.Affine(translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)}),
                    iaa.Affine(rotate=(-10, 10)),
                    iaa.Affine(shear=(-6, 6))
                ], random_order=True),
                iaa.SomeOf(randint(1, 7), [
                    iaa.Fliplr(0.5),  # horizontal flips
                    iaa.Crop(percent=(0, 0.1), keep_size=False),  # random crops
                    # Small gaussian blur with random sigma between 0 and 0.5.
                    # But we only blur about 50% of all images.
                    iaa.Sometimes(0.4,
                                  iaa.GaussianBlur(sigma=(0, 0.4))
                                  ),
                    # making the image darker or brighter.
                    iaa.Sometimes(0.5,
                                  iaa.Multiply((0.6, 1.4)),
                                  iaa.Add((-35, 35))),
                    # Strengthen or weaken the contrast in each image.
                    iaa.ContrastNormalization((0.8, 1.4)),
                    # Add gaussian noise.
                    # For 50% of all images, we sample the noise once per pixel.
                    # For the other 50% of all images, we sample the noise per pixel AND
                    # channel. This can change the color (not only brightness) of the
                    # pixels.
                    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
                    # Make some images brighter and some darker.
                    # In 20% of all cases, we sample the multiplier once per channel,
                    # which can end up changing the color of the images.
                    iaa.Multiply((0.9, 1.1), per_channel=0.2)
                ], random_order=True)
            ], random_order=True)

        elif args.medium:
            # select random number of transformations
            aug = iaa.Sequential([
                # Apply affine transformations to each image.
                # Scale/zoom them, translate/move them, rotate them and shear them.
                iaa.SomeOf(randint(2, 4), [
                    iaa.Affine(scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}),
                    iaa.Affine(translate_percent={"x": (-0.15, 0.15), "y": (-0.15, 0.15)}),
                    iaa.Affine(rotate=(-20, 20)),
                    iaa.Affine(shear=(-8, 8))
                ], random_order=True),
                iaa.SomeOf(randint(4, 7), [
                    iaa.Fliplr(0.5),  # horizontal flips
                    iaa.Crop(percent=(0, 0.1), keep_size=False),  # random crops
                    # Small gaussian blur with random sigma between 0 and 0.5.
                    # But we only blur about 50% of all images.
                    iaa.Sometimes(0.5,
                                  iaa.GaussianBlur(sigma=(0, 0.5))
                                  ),
                    # making the image darker or brighter.
                    iaa.Sometimes(0.5,
                                  iaa.Multiply((0.5, 1.5)),
                                  iaa.Add((-40, 40))),
                    # Strengthen or weaken the contrast in each image.
                    iaa.ContrastNormalization((0.75, 1.5)),
                    # Add gaussian noise.
                    # For 50% of all images, we sample the noise once per pixel.
                    # For the other 50% of all images, we sample the noise per pixel AND
                    # channel. This can change the color (not only brightness) of the
                    # pixels.
                    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
                    # Make some images brighter and some darker.
                    # In 20% of all cases, we sample the multiplier once per channel,
                    # which can end up changing the color of the images.
                    iaa.Multiply((0.8, 1.2), per_channel=0.2)
                ], random_order=True)
            ], random_order=True)

        elif args.high:
            # select random number of transformations
            aug = iaa.Sequential([
                # Apply affine transformations to each image.
                # Scale/zoom them, translate/move them, rotate them and shear them.
                iaa.Sequential([
                    iaa.Affine(scale={"x": (0.75, 1.25), "y": (0.75, 1.25)}),
                    iaa.Affine(translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}),
                    iaa.Affine(rotate=(-20, 20)),
                    iaa.Affine(shear=(-10, 10))
                ], random_order=True),
                iaa.Sequential([
                    iaa.Fliplr(0.5),  # horizontal flips
                    iaa.Crop(percent=(0, 0.2), keep_size=False),  # random crops
                    # Small gaussian blur with random sigma between 0 and 0.5.
                    # But we only blur about 50% of all images.

                    iaa.GaussianBlur(sigma=(0, 0.6)),
                    # making the image darker or brighter.
                    iaa.Sometimes(0.5,
                                  iaa.Multiply((0.4, 1.6)),
                                  iaa.Add((-45, 45))),
                    # Strengthen or weaken the contrast in each image.
                    iaa.ContrastNormalization((0.7, 1.6)),
                    # Add gaussian noise.
                    # For 50% of all images, we sample the noise once per pixel.
                    # For the other 50% of all images, we sample the noise per pixel AND
                    # channel. This can change the color (not only brightness) of the
                    # pixels.
                    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.1 * 255), per_channel=0.5),
                    # Make some images brighter and some darker.
                    # In 20% of all cases, we sample the multiplier once per channel,
                    # which can end up changing the color of the images.
                    iaa.Multiply((0.75, 1.25), per_channel=0.25)
                ], random_order=True)
            ], random_order=True)
        else:
            print("Nothing to do, please use --low, --medium or --high option")
            exit(0)
        seq_det = aug.to_deterministic()

        # augment image
        image_aug = seq_det.augment_image(image)

        # augment bbox tooq
        bbs_aug = seq_det.augment_bounding_boxes([bbs])[0]

        # show augmented image
        if args.show:
            ia.imshow(bbs_aug.draw_on_image(image_aug, thickness=2))

        # and save it
        directory = os.path.join(args.output, os.path.split(os.path.relpath(image_path, args.root))[0])
        if not os.path.exists(directory):
            os.makedirs(directory)
        output_image_path = os.path.join(directory, "aug_" + os.path.basename(image_path))
        output_txt_path = os.path.join(directory, "aug_" + os.path.basename(txt_path))
        misc.imsave(output_image_path, image_aug)

        # print YOLO bboxes to file
        with open(output_txt_path, "w") as yolo_bb_output:
            for j in range(len(bbs.bounding_boxes)):
                bbox = bbs_aug.bounding_boxes[j]
                size = image_aug.shape
                box_width = abs(bbox.x2 - bbox.x1) / size[1]
                box_height = abs(bbox.y2 - bbox.y1) / size[0]

                box_x_center = (abs(bbox.x1 + bbox.x2) / size[1]) / 2
                box_y_center = (abs(bbox.y1 + bbox.y2) / size[0]) / 2

                yolo_bb_output.write("0 %f %f %f %f\n" % (box_x_center, box_y_center, box_width, box_height))

        paths.write(output_image_path + "\n")
