#
#   Author: Tomas Libal
#   Date:   7.11.2018
#
#   This script will create from input file with paths to images 3 another files (train, test, valid)
#

import os
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter
from random import randint


parser = ArgumentParser(description="Creator of test.txt, train.txt and validate.txt files ", epilog="Author: Tomas Libal",
                        formatter_class=RawTextHelpFormatter)

parser.add_argument("-i", help="Input txt file", action="store", dest="input", required=True)
parser.add_argument("-o", help="Output path for txt files", action="store", dest="output")
parser.add_argument("-d", help="Delete files if exists", action="store_true", dest='delete')

args = parser.parse_args()


if args.output is None:
    args.output = os.path.dirname(os.path.abspath(__file__))

if os.path.exists(os.path.join(args.output, "train.txt")) and args.delete:
    os.remove(os.path.join(args.output, "train.txt"))

if os.path.exists(os.path.join(args.output, "test.txt")) and args.delete:
    os.remove(os.path.join(args.output, "test.txt"))

if os.path.exists(os.path.join(args.output, "validate.txt")) and args.delete:
    os.remove(os.path.join(args.output, "validate.txt"))

train_txt = open(os.path.join(args.output, "train.txt"), "a")
test_txt = open(os.path.join(args.output, "test.txt"), "a")
validate_txt = open(os.path.join(args.output, "validate.txt"), "a")

with open(args.input, "r") as input_file:
    lines = input_file.readlines()
    input_file.close()

for line in lines:
    random_value = randint(0, 99)
    if random_value < 10:
        test_txt.write(line)
    elif random_value < 20:
        validate_txt.write(line)
    else:
        train_txt.write(line)

test_txt.close()
train_txt.close()
validate_txt.close()
