#
#   Author: Tomas Libal
#   Date:   10.4.2019
#
#   This program will show roc curve based on results from validation dataset.
#

import json
import pickle
import os
import subprocess
import itertools
import re

from math import sqrt

from bokeh.io import output_file, show
from bokeh.plotting import figure, save
from bokeh.models import ColumnDataSource, HoverTool
from bokeh.palettes import Dark2_5 as palette

from argparse import ArgumentParser
from argparse import RawTextHelpFormatter


# Calculate from coords
def overlap(x1, w1, x2, w2):
    l1 = float(x1) - float(w1) / 2
    l2 = float(x2) - float(w2) / 2
    max_l = max(l1, l2)
    r1 = float(x1) + float(w1) / 2
    r2 = float(x2) + float(w2) / 2
    min_r = min(r1, r2)
    return min_r - max_l


# Calculate intersection of two given boxes
def box_intersection(box_a, box_b):
    w = overlap(box_a[0], box_a[2], box_b[0], box_b[2])
    h = overlap(box_a[1], box_a[3], box_b[1], box_b[3])
    if w < 0 or h < 0:
        return 0
    area = w * h
    return area


# Calculate union of two given boxes
def box_union(box_a, box_b):
    i = box_intersection(box_a, box_b)
    u = float(box_a[2]) * float(box_a[3]) + float(box_b[2]) * float(box_b[3]) - i
    return u


# Calculate IoU value between two given boxes
def box_iou(box_a, box_b):
    return box_intersection(box_a, box_b) / box_union(box_a, box_b)


# Calculate data for the output plot
def calculate_data(count_fp, count_tp, count_fn, count_tn, count_images):
    results = {}
    positives = count_tp + count_fn
    negatives = count_tn + count_fp
    if count_fn == 0:
        results['TPR'] = 1.0
    else:
        results['TPR'] = count_tp / positives

    if positives + negatives == 0:
        results['ACC'] = 1.0
    else:
        results['ACC'] = (count_tp + count_tn) / (positives + negatives)

    results['FPPI'] = count_fp / count_images
    results['ERR'] = 1 - results['ACC']

    return results


# Get and return basename from given file path
def get_basename(full_filename):
    return os.path.splitext(os.path.basename(full_filename))[0]


# Create plot with captions and return it
def create_plot():
    plot = figure(
        title="ROC curve",
        active_scroll="wheel_zoom",
        x_axis_label='False Positive Per Image', y_axis_label='True Positive Rate',
        sizing_mode='stretch_both'
    )
    plot.output_backend = "svg"
    return plot


# Set legend in given plot to right bottom corner and add action on click and return it
def set_legend(plot):
    plot.legend.location = "bottom_right"
    plot.legend.click_policy = "hide"
    return plot


parser = ArgumentParser(description="This program will show roc curve based on results from validation dataset.",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)
subparsers = parser.add_subparsers(dest="subcommand")

parse_pkl = subparsers.add_parser("parse_pkl", help="Parse input .pkl file and draw one graph with all ROCs",
                                  description="Parse input .pkl file and draw one graph with all ROCs",
                                  formatter_class=RawTextHelpFormatter)

parse_pkl.add_argument("-d", "--dir", help="Path to dir with input .pkl files\nIf not set, .pkl files "
                                           "will be loaded from stdin", action="store", dest="dir")
parse_pkl.add_argument("-o", "--out", help="Path to output directory, in which will be graph saved",
                       action="store", dest="output", default="/home/xlibal00/Documents/BP/roc_graphs/")
parse_pkl.add_argument("-f", "--file", help="Output file name (without extension), if --separately, -f is unused",
                       action="store", dest="output_file", default="multiple_roc")
parse_pkl.add_argument("-s", "--show", help="Save and show ROC curve as .html (default: only save)",
                       action="store_true", dest="show", default=False)
parse_pkl.add_argument("--sizes", help="Draw 3 lines in graph (one for each size category)",
                       action="store_true", dest="sizes", default=False)
mex = parse_pkl.add_mutually_exclusive_group()
mex.add_argument("--all", help="Create one graph with ROC from all pkls",
                 action="store_true", dest="all", default=False)
mex.add_argument("--separately", help="Create one graph with ROC for each pkl individually",
                 action="store_true", dest="separately", default=False)

roc = subparsers.add_parser("roc", help="Parse input .json file and draw ROC",
                            description="Parse input .json file and draw ROC",
                            formatter_class=RawTextHelpFormatter)

roc.add_argument("-i", "--in", help="Path to input json file", action="store", dest="input", required=True)
roc.add_argument("-o", "--out", help="Path to output directory, in which will be graph saved",
                 action="store", dest="output", default="/home/xlibal00/Documents/BP/roc_graphs/")
roc.add_argument("-w", "--weights", help=".weights file (result of training) if debug, otherwise part of outputfile "
                                         "name", action="store", dest="weight", required=True)

roc.add_argument("-d", "--debug", help="Draw images",
                 action="store_true", dest="debug", default=False)
roc.add_argument("--sizes", help="Draw 3 lines in graph (one for each size category)",
                 action="store_true", dest="sizes", default=False)
roc_meg = roc.add_mutually_exclusive_group()

roc_meg.add_argument("--save", help="Save ROC data into output .pkl file", action="store_true", dest="save",
                     default=False)
roc_meg.add_argument("-s", "--show", help="Save and show ROC curve, otherwise it will be only saved",
                     action="store_true", dest="show", default=False)
roc.add_argument("-r", "--recursive", help="Loop through all .json files in given dir", dest="recursive",
                 action="store_true", default=False)

validate = subparsers.add_parser("validate", help="Valid input dataset with given weight and create .json file",
                                 description="Valid input dataset with given weight and create .json file",
                                 formatter_class=RawTextHelpFormatter)

validate.add_argument("-d", "--data", action="store", required=False,
                      help="Data file (default: /home/xlibal00/Documents/BP/darknet/cfg/data/universal.data)",
                      dest="data", default="/home/xlibal00/Documents/BP/darknet/cfg/data/universal.data")
validate.add_argument("-c", "--cfg", help=".cfg file", dest="cfg", action="store", required=True)
validate.add_argument("-w", "--weights", help=".weights file (or dir with .weights files if recursive)", dest="weights",
                      action="store", required=True)
validate.add_argument("-p", "--prefix", help="Output directory (default: /home/xlibal00/Documents/BP/darknet/results/)",
                      dest="output", action="store", default="/home/xlibal00/Documents/BP/darknet/results/",
                      required=False)
validate.add_argument("-o", "--out", help="output file name (without extension)\n(If recursive is True, weight number "
                                          "will be appended to end of name)", dest="out", action="store",
                      required=True)
validate.add_argument("-v", "--valid", help="File with paths to dataset", dest="valid", action="store", required=True)
validate.add_argument("-r", "--recursive", help="Loop through all .weights files in given dir", dest="recursive",
                      action="store_true", default=False)
validate.add_argument("-m", "--modulo", help="Skip all weight files which mod is not 0", dest="modulo",
                      action="store", type=int, default=1)

args = parser.parse_args()

# if output file not ends with /
if not args.output.endswith('/'):
    args.output = args.output + "/"

# Create ROC curve and save graph values to .pkl file
if args.subcommand == "roc":
    json_list = []
    if os.path.isdir(args.output):
        if args.recursive:
            if os.path.isdir(args.input):
                for json_file in os.listdir(args.input):
                    if json_file.endswith(".json"):
                        json_list.append(os.path.join(args.input, json_file))
                    else:
                        continue
            else:
                print("If recursive, input must be a dir")
                exit(1)
        else:
            if os.path.isfile(args.input):
                json_list.append(args.input)
            else:
                print("If is not recursive, input must be a file")
                exit(1)
    else:
        print("Output must be a dir")
        exit(1)

    for json_file in json_list:
        try:
            images = json.load(open(json_file, "r"))
        except ValueError:
            print("Cannot parse: " + json_file)
            continue

        print_results = True
        # if args.header == "Y":

        if args.sizes:
            print("input_file,thresh,FP,TP_50,TP_50_150,TP_150,FN_50,FN_50_150,FN_150,TN,TPR_50,TPR_50_150,TPR_150,"
                  "FPPI_50,FPPI_50_150,FPPI_150,ACC_50,ACC_50_150,ACC_150,ERR_50,ERR_50_150,ERR_150")
        else:
            print("input_file,thresh,FP,TP,FN,TN,TPR,FPPI,ACC,ERR")

        graph_values_50 = {'FPPI': [], 'TPR': []}  # values for graph
        graph_values_50_150 = {'FPPI': [], 'TPR': []}  # values for graph
        graph_values_150 = {'FPPI': [], 'TPR': []}  # values for graph
        graph_values = {'FPPI': [], 'TPR': []}  # values for graph

        if args.recursive:
            basename = args.weight + "_" + get_basename(json_file)
        else:
            basename = get_basename(args.weight) + "_" + get_basename(args.input)

        if args.sizes:
            basename = basename + "_roc_sizes"
        else:
            basename = basename + "_roc"

        for thresh in range(0, 101):
            # if thresh == 100:
            #     thresh = 99.9
            thresh = float(thresh) / 100

            TP_50 = FN_50 = 0.0
            TP_50_150 = FN_50_150 = 0.0
            TP_150 = FN_150 = 0.0
            FP = TP = FN = TN = 0.0

            for image in images:
                FN_backup = FN
                image['detections'][:] = [detection for detection in image['detections'] if
                                          not detection['score'] <= thresh]
                for detection in image['detections']:
                    detection['iou'] = 0.0

                image["original"] = []
                txt_path = os.path.splitext(image["image_path"])[0].replace("//", "/") + ".txt"
                for line in open(txt_path, "r").readlines():
                    coords = line.split()
                    del coords[0]

                    left = int((float(coords[0]) - float(coords[2]) / 2.0) * image["image_width"])
                    right = int((float(coords[0]) + float(coords[2]) / 2.0) * image["image_width"])
                    top = int((float(coords[1]) - float(coords[3]) / 2.0) * image["image_height"])
                    bottom = int((float(coords[1]) + float(coords[3]) / 2.0) * image["image_height"])

                    if left < 0:
                        left = 0
                    if right > image["image_width"] - 1:
                        right = image["image_width"] - 1
                    if top < 0:
                        top = 0
                    if bottom > image["image_height"] - 1:
                        bottom = image["image_height"] - 1

                    image["original"].append({"left": left, "right": right, "top": top, "bottom": bottom,
                                              "width": right - left, "used": False})
                    if args.debug:
                        print("left: " + str(left) + ", right: " + str(right) + ", top: " + str(top) + ", bot: " + str(
                            bottom) +
                              ", width: " + str(right - left))

                    for detection_bbox in image["detections"]:
                        if args.debug:
                            print("x: " + str(detection_bbox['yolo_cords'][0]) + ", y: " + str(
                                detection_bbox['yolo_cords'][1])
                                  + ", w: " + str(detection_bbox['yolo_cords'][2]) + ", h: "
                                  + str(detection_bbox['yolo_cords'][3]))

                        iou = box_iou(coords, detection_bbox['yolo_cords'])

                        # return the intersection over union value
                        if detection_bbox["iou"] < iou:
                            detection_bbox["used"] = True
                            detection_bbox["iou"] = iou
                            detection_bbox["val_bbox_width"] = image['original'][-1]['width']
                            image['original'][-1]['used'] = True

                if args.debug:
                    print("\nThresh: " + str(thresh))
                    if args.sizes:
                        print("BEFORE:\nFP: " + str(FP) + ", TP_50: " + str(TP_50) + ", TP_50_150: " + str(TP_50_150)
                              + ", TP_150: " + str(TP_150) + ", FN_50: " + str(FN_50) + ", FN_50_150: " + str(FN_50_150)
                              + ", FN_150: " + str(FN_150) + ", TN: " + str(TN) + "\n")
                    else:
                        print(
                            "BEFORE:\nFP: " + str(FP) + ", TP: " + str(TP) + ", FN: " + str(FN) + ", TN: " + str(TN) + "\n")

                # calculate values for confusion matrix
                for detection in image["detections"]:
                    if detection["used"] is False:
                        FP += 1
                    else:
                        if detection["iou"] < 0.5:
                            FP += 1
                        else:
                            if args.sizes:
                                if detection["val_bbox_width"] <= 50:
                                    TP_50 += 1
                                elif 50 < detection["val_bbox_width"] <= 150:
                                    TP_50_150 += 1
                                elif detection["val_bbox_width"] > 150:
                                    TP_150 += 1
                            else:
                                TP += 1

                # calculate values for confusion matrix
                for detection in image['original']:
                    if detection["used"] is False:
                        if args.sizes:
                            if detection["width"] <= 50:
                                FN_50 += 1
                            elif 50 < detection["width"] <= 150:
                                FN_50_150 += 1
                            elif detection["width"] > 150:
                                FN_150 += 1
                        else:
                            FN += 1

                if (len(image['detections']) == 0) and (len(image['original']) == 0):
                    TN += 1

                if args.debug:
                    if args.sizes:
                        print("AFTER:\nFP: " + str(FP) + ", TP_50: " + str(TP_50) + ", TP_50_150: " + str(TP_50_150)
                              + ", TP_150: " + str(TP_150) + ", FN_50: " + str(FN_50) + ", FN_50_150: " + str(FN_50_150)
                              + ", FN_150: " + str(FN_150) + ", TN: " + str(TN) + "\n")
                    else:
                        print(
                            "AFTER:\nFP: " + str(FP) + ", TP: " + str(TP) + ", FN: " + str(FN) + ", TN: " + str(TN) + "\n")
                    if FN_backup != FN:
                        if os.path.isfile(args.weight):
                            p = subprocess.Popen(["/home/xlibal00/Documents/BP/darknet/darknet", "detector", "test",
                                                  "-data", "/home/xlibal00/Documents/BP/darknet/cfg/data/universal.data",
                                                  "-cfg", "/home/xlibal00/Documents/BP/darknet/cfg/cfg/spz_test_1440.cfg",
                                                  "-weights", args.weight,
                                                  "-filename", image["image_path"],
                                                  "-thresh", "0.005"], stderr=subprocess.PIPE)
                            p.wait()
                        else:
                            print("Path is not a file")
                    print("***********************************************************************************")

            if args.sizes:
                graph_data_50 = calculate_data(FP, TP_50, FN_50, TN, len(images))
                graph_data_50_150 = calculate_data(FP, TP_50_150, FN_50_150, TN, len(images))
                graph_data_150 = calculate_data(FP, TP_150, FN_150, TN, len(images))

                graph_values_50['TPR'].append(graph_data_50['TPR'])
                graph_values_50['FPPI'].append(graph_data_50['FPPI'])
                graph_values_50_150['TPR'].append(graph_data_50_150['TPR'])
                graph_values_50_150['FPPI'].append(graph_data_50_150['FPPI'])
                graph_values_150['TPR'].append(graph_data_150['TPR'])
                graph_values_150['FPPI'].append(graph_data_150['FPPI'])
            else:
                graph_data = calculate_data(FP, TP, FN, TN, len(images))
                graph_values['TPR'].append(graph_data['TPR'])
                graph_values['FPPI'].append(graph_data['FPPI'])

            if print_results:
                if args.sizes:
                    print(os.path.join(args.input, args.input) + "," + str(thresh) + "," + str(int(FP)) + "," +
                          str(int(TP_50)) + ',' + str(int(TP_50_150)) + ',' + str(int(TP_150)) + ',' +
                          str(int(FN_50)) + ',' + str(int(FN_50_150)) + ',' + str(int(FN_150)) + ',' +
                          str(int(TN)) + "," + str(graph_data_50['TPR']) + "," + str(graph_data_50_150['TPR']) + "," +
                          str(graph_data_150['TPR']) + "," + str(graph_data_50['FPPI']) + "," +
                          str(graph_data_50_150['FPPI']) + "," + str(graph_data_150['FPPI']) + "," +
                          str(graph_data_50['ACC']) + ',' + str(graph_data_50_150['ACC']) + ',' +
                          str(graph_data_150['ACC']) + "," + str(graph_data_50['ERR']) + ',' +
                          str(graph_data_50_150['ERR']) + ',' + str(graph_data_150['ERR']))
                else:
                    print(os.path.join(args.input, args.input) + "," + str(thresh) + "," + str(int(FP)) + "," +
                          str(int(TP)) + "," + str(int(FN)) + "," + str(int(TN)) + "," + str(graph_data['TPR']) + "," +
                          str(graph_data['FPPI']) + "," + str(graph_data['ACC']) + "," + str(graph_data['ERR']))

        if args.save is False:
            # output to static HTML file
            output_file(args.output + basename + ".html", title=basename + ".html")

            # create a new plot
            p = create_plot()
            p.output_backend = "svg"

            if args.sizes:
                source_50 = ColumnDataSource(data=graph_values_50)
                source_50_150 = ColumnDataSource(data=graph_values_50_150)
                source_150 = ColumnDataSource(data=graph_values_150)
                p.line(x='FPPI', y='TPR', legend="LP <= 50px", color="blue", line_width=1.5, source=source_50)
                p.circle(x='FPPI', y='TPR', legend="LP <= 50px", fill_color="blue", line_color="blue", size=4,
                         source=source_50)

                p.line(x='FPPI', y='TPR', legend="50px < LP <= 150px", color="red", line_width=1.5, source=source_50_150)
                p.square(x='FPPI', y='TPR', legend="50px < LP <= 150px", fill_color="red", line_color="red", size=4,
                         source=source_50_150)

                p.line(x='FPPI', y='TPR', legend="LP > 150px", color="green", line_width=1.5, source=source_150)
                p.cross(x='FPPI', y='TPR', legend="LP > 150px", fill_color="green", line_color="green", size=4,
                        source=source_150)
            else:
                source = ColumnDataSource(data=graph_values)
                p.line(x='FPPI', y='TPR', legend="All sizes", color="blue", line_width=1.5, source=source)
                p.circle(x='FPPI', y='TPR', legend="All sizes", fill_color="blue", line_color="blue", size=4, source=source)

            p = set_legend(p)

            if args.show:
                # save and show the results
                show(p)
            else:
                # save the results
                save(p)
        else:
            # save to .pkl file
            with open(args.output + basename + ".pkl", 'wb') as fp:
                if args.sizes:
                    pickle.dump(graph_values_50, fp)
                    pickle.dump(graph_values_50_150, fp)
                    pickle.dump(graph_values_150, fp)
                else:
                    pickle.dump(graph_values, fp)
                fp.close()

# parse given pkl and create plot from given data
elif args.subcommand == "parse_pkl":
    pkl_files = []
    if args.dir:
        for filename in os.listdir(args.dir):
            if filename.endswith(".pkl"):
                if args.sizes and "sizes" in filename:
                    pkl_files.append(os.path.join(args.dir, filename))
                elif not args.sizes and "sizes" not in filename:
                    pkl_files.append(os.path.join(args.dir, filename))
            else:
                continue
    else:
        filename = ''
        while True:
            filename = raw_input("Input .pkl file (n/no for end): ")
            if filename in ['n', 'N', 'no', 'NO', 'No']:
                break
            else:
                pkl_files.append(filename)
    pkl_files.sort()

    # create one graph with all ROC curves inside
    if args.all:
        output_file(args.output + args.output_file + ".html", title=args.output_file)

        # create a new plot
        p = create_plot()
        p.output_backend = "svg"
        colors = itertools.cycle(palette)

        for file, color in zip(pkl_files, colors):
            pkl_file = open(file, "rb")
            if args.sizes:
                source_50 = ColumnDataSource(data=pickle.load(pkl_file))
                source_50_150 = ColumnDataSource(data=pickle.load(pkl_file))
                source_150 = ColumnDataSource(data=pickle.load(pkl_file))
                p.line(x='FPPI', y='TPR', legend=get_basename(file) + " LP <= 50px", color=color, line_width=1.5,
                       source=source_50)
                p.circle(x='FPPI', y='TPR', legend=get_basename(file) + " LP <= 50px", fill_color=color,
                         line_color=color, size=4, source=source_50)

                p.line(x='FPPI', y='TPR', legend=get_basename(file) + " 50px < LP <= 150px", color=color,
                       line_width=1.5, source=source_50_150)
                p.square(x='FPPI', y='TPR', legend=get_basename(file) + " 50px < LP <= 150px", fill_color=color,
                         line_color=color, size=4, source=source_50_150)

                p.line(x='FPPI', y='TPR', legend=get_basename(file) + " LP > 150px", color=color, line_width=1.5,
                       source=source_150)
                p.cross(x='FPPI', y='TPR', legend=get_basename(file) + " LP > 150px", fill_color=color,
                        line_color=color, size=4, source=source_150)
            else:
                source = ColumnDataSource(data=pickle.load(pkl_file))
                p.line(x='FPPI', y='TPR', legend=get_basename(file), color=color, line_width=1.5, source=source)
                circles = p.circle(x='FPPI', y='TPR', legend=get_basename(file), fill_color=color, line_color=color, size=4,
                                   source=source)

                p.add_tools(
                    HoverTool(
                        tooltips=[("weight", get_basename(file))],
                        mode="mouse",
                        renderers=[circles]
                    )
                )
            p = set_legend(p)
        if args.show:
            # save and show the results
            show(p)
        else:
            # save the results
            save(p)

    # create graph for each pkl file
    elif args.separately:
        for file in pkl_files:
            if args.sizes:
                output_file(args.output + get_basename(file) + "_roc_sizes.html", title=get_basename(file)+"_roc_sizes")
            else:
                output_file(args.output + get_basename(file) + "_roc.html", title=get_basename(file)+"_roc")

            # create a new plot
            p = create_plot()
            p.output_backend = "svg"
            pkl_file = open(file, "rb")
            if args.sizes:
                source_50 = ColumnDataSource(data=pickle.load(pkl_file))
                source_50_150 = ColumnDataSource(data=pickle.load(pkl_file))
                source_150 = ColumnDataSource(data=pickle.load(pkl_file))
                p.line(x='FPPI', y='TPR', legend="LP <= 50px", color="blue", line_width=1.5, source=source_50)
                p.circle(x='FPPI', y='TPR', legend="LP <= 50px", fill_color="blue", line_color="blue", size=4,
                         source=source_50)

                p.line(x='FPPI', y='TPR', legend="50px < LP <= 150px", color="red", line_width=1.5, source=source_50_150)
                p.square(x='FPPI', y='TPR', legend="50px < LP <= 150px", fill_color="red", line_color="red", size=4,
                         source=source_50_150)

                p.line(x='FPPI', y='TPR', legend="LP > 150px", color="green", line_width=1.5, source=source_150)
                p.cross(x='FPPI', y='TPR', legend="LP > 150px", fill_color="green", line_color="green", size=4,
                        source=source_150)
            else:
                source = ColumnDataSource(data=pickle.load(pkl_file))
                p.line(x='FPPI', y='TPR', legend="All sizes", color="blue", line_width=1.5, source=source)
                p.circle(x='FPPI', y='TPR', legend="All sizes", fill_color="blue", line_color="blue", size=4, source=source)

            p = set_legend(p)

            if args.show:
                # save and show the results
                show(p)
            else:
                # save the results
                save(p)
    else:
        print("Nothing to do")

# run validation above neural network with given weight(s) and create output json files
elif args.subcommand == "validate":
    if os.path.isdir(args.output):
        if args.recursive:
            if os.path.isdir(args.weights):
                for weight_file in os.listdir(args.weights):
                    try:
                        found = re.search("_([0-9]*)\.", weight_file).group(1)
                    except AttributeError:
                        continue
                    if weight_file.endswith(".weights") and int(found) % args.modulo == 0:
                        p = subprocess.Popen(["/home/xlibal00/Documents/BP/darknet/darknet", "detector", "valid",
                                              "-data", args.data,
                                              "-cfg", args.cfg,
                                              "-weights", os.path.join(args.weights, weight_file),
                                              "-prefix", args.output,
                                              "-out", args.out + "_" + get_basename(weight_file),
                                              "-valid", args.valid])
                        p.wait()
                    else:
                        continue
            else:
                print("Dir is TRUE, weight path is not directory")
                exit(1)
        else:
            p = subprocess.Popen(["/home/xlibal00/Documents/BP/darknet/darknet", "detector", "valid",
                                  "-data", args.data,
                                  "-cfg", args.cfg,
                                  "-weights", args.weights,
                                  "-prefix", args.output,
                                  "-out", args.out,
                                  "-valid", args.valid])
            p.wait()
    else:
        print("Prefix is not a dir")
        exit(1)
else:
    print("Unknown subcommand")
    exit(1)

