#
#   Author: Tomas Libal
#   Date:   9.10.2018
#
#   This script will create train or test file with paths to images in dataset
#

import os
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter


parser = ArgumentParser(description="Creator of test.txt and train.txt files ", epilog="Author: Tomas Libal",
                        formatter_class=RawTextHelpFormatter)

parser.add_argument("--test", help="Create test.txt", action="store_true")
parser.add_argument("--train", help="Create train.txt", action="store_true")
parser.add_argument("-i", help="Input file", action="store", dest="input")
parser.add_argument("-o", help="Output file", action="store", dest="output")

args = parser.parse_args()

if args.input is None:
    args.input = os.path.dirname(os.path.abspath(__file__))

if args.output is None:
    args.output = os.path.dirname(os.path.abspath(__file__))

if args.train and os.path.exists(os.path.join(args.output, "train.txt")):
    os.remove(os.path.join(args.output, "train.txt"))

if args.test and os.path.exists(os.path.join(args.output, "test.txt")):
    os.remove(os.path.join(args.output, "test.txt"))

# get path for each jpg file in all subdirs given directory 
for subdir, dirs, files in os.walk(os.path.abspath(args.input)):
    for image in files:
        filename, extension = os.path.splitext(image)
        if extension == ".jpg":
            path = os.path.abspath(os.path.join(subdir, image))
            if args.train:
                with open(os.path.join(args.output, "train.txt"), "a") as myfile:
                    myfile.write(path + "\n")
                    myfile.close()
            if args.test:
                with open(os.path.join(args.output, "test.txt"), "a") as myfile:
                    myfile.write(path + "\n")
                    myfile.close()
        else:
            continue

