# import os
# import sys
# from math import sqrt
# from bokeh.io import output_file, show
# from bokeh.plotting import figure, save
# from bokeh.models import ColumnDataSource
# from argparse import ArgumentParser
# from argparse import RawTextHelpFormatter
#
# parser = ArgumentParser(description="This program will show roc curve based on results from validation dataset.",
#                         epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)
#
# parser.add_argument("-v", "--validate", help="This will print only first threshold results", action="store_true",
#                     default=False)
# parser.add_argument("-i", "--input", help="Path to input txt file", action="store", dest="input")
# parser.add_argument("-o", "--output", help="Path to output directory", action="store", dest="output",
#                     default="/home/xlibal00/Documents/BP/roc_graphs/")
# parser.add_argument("-s", "--show", help="Save and show ROC curve, otherwise it will be only saved",
#                     action="store_true", dest="show", default=False)
# parser.add_argument("--header", help="Print header to output ('Y'/'N')", action="store", dest="header", default="N")
#
# args = parser.parse_args()
#
#
# # Read input file and create list of dictionaries with validation data
# def parse_data(data, delete_last):
#     data_list = []
#     skip = True
#     for line in data:
#         # skip empty line
#         if line == "":
#             continue
#         # get image path
#         elif line.startswith("Image path: ") and not any(d['image'] == line[12:] for d in data_list):
#             data_list.append({'image': line[12:], 'detections': [], 'leftover_LPs': []})
#             skip = False
#         # get IOU and probability
#         elif line.startswith("IoU: ") and line[15:27] == "probability:" and not skip:
#             data_list[len(data_list) - 1]['detections'].append({'iou': line[5:11], 'probability': line[28:delete_last]})
#         elif line.startswith("licence_plate: ") and not skip:
#             if not any(d['probability'] == line[15:delete_last] for d in data_list[len(data_list) - 1]['detections']):
#                 data_list[len(data_list) - 1]['leftover_LPs'].append(line[15:delete_last])
#         # skip all other lines
#         else:
#             skip = True
#     return data_list
#
#
# # if not input, read from stdin
# if args.input is None:
#     args.input = "STDIN"
#     input_data = sys.stdin
#     images = parse_data(input_data, -2)
#     basename = "roc.html"
# # open file, get his name and read from it
# else:
#     input_data = open(args.input, "r").read()
#     lines = input_data.split('\n')
#     images = parse_data(lines, -1)
#     basename = "roc_" + os.path.splitext(os.path.basename(args.input))[0] + ".html"
#
# print_results = True
# if args.header == "Y":
#     print("path,thresh,TP,TN,FP,FN,TPR,FNR,FPPI,ACC")
#
# graph_values = {'FPPI': [], 'FNR': [], 'TPR': []}  # values for graph
# # graph_values['FPR'].append(1.0)
# # graph_values['TPR'].append(1.0)
#
#
# # loop through all threshes
# for thresh in range(0, 101):
#     TP = TN = FP = FN = image_counter = 0.0
#     for image in images:
#         image_counter += 1
#         for leftover in image['leftover_LPs']:
#             if int(leftover) >= thresh:
#                 FP += 1
#
#         if (len(image['detections']) == 0) and (len(image['leftover_LPs']) == 0):
#             TN += 1
#
#         for detection in image['detections']:
#             if int(detection['probability']) == -1 and float(detection['iou']) == 0.0:
#                 FN += 1
#             elif float(detection['iou']) < 0.6 and int(detection['probability']) >= thresh:
#                 FN += 1
#             else:
#                 if int(detection['probability']) >= thresh:
#                     TP += 1
#                 elif int(detection['probability']) < thresh:
#                     FN += 1
#
#     P = TP + FN
#     N = TN + FP
#     TPR = TP / P
#
#     TNR = TN / N
#     PPV = TP / (TP + FP)
#     NPV = TN / (TN + FN)
#     FNR = FN / P
#
#     FPR = FP / N
#
#     FDR = FP / (FP + TP)
#     FOR = FN / (FN + TN)
#
#     ACC = (TP + TN) / (P + N)
#
#     F1 = 2 * ((PPV * TPR) / (PPV + TPR))
#     MCC = ((TP * TN - FP * FN) / sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)))
#
#     FPPI = FP / image_counter
#
#     if print_results:
#         print(args.input + "," + str(thresh) + "," + str(int(TP)) + "," + str(int(TN)) + "," + str(int(FP)) + "," +
#               str(int(FN)) + "," + str(TPR) + "," + str(FNR) + "," + str(FPPI) + "," + str(ACC))
#         # print("Thresh: " + str(thresh))
#         # print("True Positive (TP): " + str(int(TP)))
#         # print("True Negative (TN): " + str(int(TN)))
#         # print("False Positive (FP): " + str(int(FP)))
#         # print("False Negative (FN): " + str(int(FN)))
#         # print("")
#         # print("sensitivity, recall, hit rate, or true positive rate (TPR): " + str(TPR))
#         # # print("specificity, selectivity or true negative rate (TNR): " + str(TNR))
#         # # print("precision or positive predictive value (PPV): " + str(PPV))
#         # # # print("negative predictive value (NPV): " + str(NPV))
#         # print("miss rate or false negative rate (FNR): " + str(FNR))
#         # # print("fall-out or false positive rate (FPR): " + str(FPR))
#         # # print("false discovery rate (FDR): " + str(FDR))
#         # # # print("false omission rate (FOR): " + str(FOR))
#         # # print("")
#         # print("accuracy (ACC): " + str(ACC))
#         # # print("F1 score: " + str(F1))
#         # # # print("Matthews correlation coefficient (MCC): " + str(MCC))
#         # print("false positive per image or FPPI: " + str(FPPI))
#         # print("")
#
#     if args.validate:
#         print_results = False
#
#     graph_values['FNR'].append(FPR)
#     graph_values['TPR'].append(TPR)
#     graph_values['FPPI'].append(FPPI)
#
# # graph_values['FPR'].append(0.0)
# # graph_values['TPR'].append(0.0)
#
# # output to static HTML file
# output_file(args.output + basename)
#
# source = ColumnDataSource(data=graph_values)
#
# # p = figure(plot_width=400, plot_height=400, tooltips=TOOLTIPS,
# #            title="Mouse over the dots")
#
# # create a new plot
# p = figure(
#     title="ROC curve",
#     active_scroll="wheel_zoom",
#     x_axis_label='False Positive Per Image', y_axis_label='True Positive Rate',
#     sizing_mode='stretch_both',
#     # x_range=(0, 1), y_range=(0, 1)
# )
# x_center = [0, 1]
# y_center = [0, 1]
#
# # add some renderers
# p.line(x='FPPI', y='TPR', legend="[FPPI:TPR]", color="blue", line_width=1.5, source=source)
# p.circle(x='FPPI', y='TPR', legend="[FPPI:TPR]", fill_color="blue", line_color="blue", size=4, source=source)
# p.line(x='FPPI', y='FNR', legend="[FPPI:FNR]", color="red", line_width=1.5, source=source)
# p.square(x='FPPI', y='FNR', legend="[FPPI:FNR]", fill_color="red", line_color="red", size=4, source=source)
# # p.line(x_center, y_center, line_color='brown', line_dash='4 4')
#
# if args.show:
#     # save and show the results
#     show(p)
# else:
#     # save the results
#     save(p)
import os
import sys
from math import sqrt
from bokeh.io import output_file, show
from bokeh.plotting import figure, save
from bokeh.models import ColumnDataSource
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

parser = ArgumentParser(description="This program will show roc curve based on results from validation dataset.",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)

parser.add_argument("-v", "--validate", help="This will print only first threshold results", action="store_true",
                    default=False)
parser.add_argument("-i", "--input", help="Path to input txt file", action="store", dest="input")
parser.add_argument("-o", "--output", help="Path to output directory", action="store", dest="output",
                    default="/home/xlibal00/Documents/BP/roc_graphs/")
parser.add_argument("-s", "--show", help="Save and show ROC curve, otherwise it will be only saved",
                    action="store_true", dest="show", default=False)
parser.add_argument("--header", help="Print header to output ('Y'/'N')", action="store", dest="header", default="N")

args = parser.parse_args()


# Read input file and create list of dictionaries with validation data
def parse_data(data, delete_last):
    data_list = []
    skip = True
    for line in data:
        # skip empty line
        if line == "":
            continue
        # get image path
        elif line.startswith("Image path: ") and not any(d['image'] == line[12:] for d in data_list):
            data_list.append({'image': line[12:], 'detections': [], 'leftover_LPs': []})
            skip = False
        # get IOU and probability
        elif line.startswith("IoU: ") and line[15:27] == "probability:" and not skip:
            data_list[len(data_list) - 1]['detections'].append({'iou': line[5:11], 'probability': line[28:delete_last]})
        elif line.startswith("licence_plate: ") and not skip:
            if not any(d['probability'] == line[15:delete_last] for d in data_list[len(data_list) - 1]['detections']):
                data_list[len(data_list) - 1]['leftover_LPs'].append(line[15:delete_last])
        # skip all other lines
        else:
            skip = True
    return data_list


print_results = True
if args.header == "Y":
    print("path,TP,TN,FP,FN,TPR,FNR,FPPI,ACC")

graph_values = {'FPPI': [], 'FNR': [], 'TPR': []}  # values for graph
basename = "roc_konzultace.html"

list_of_files = []
for filename in os.listdir(args.input):
    if not filename.endswith(".txt"):
        continue
    list_of_files.append(filename)

list_of_files.sort()

for filename in list_of_files:
    if not filename.endswith(".txt"):
        continue
    input_data = open(os.path.join(args.input, filename), "r").read()
    lines = input_data.split('\n')
    images = parse_data(lines, -1)
    # basename = "roc_" + os.path.splitext(os.path.basename(args.input))[0] + ".html"

    TP = TN = FP = FN = image_counter = 0.0
    for image in images:
        image_counter += 1
        for leftover in image['leftover_LPs']:
            FP += 1

        if (len(image['detections']) == 0) and (len(image['leftover_LPs']) == 0):
            TN += 1

        for detection in image['detections']:
            if int(detection['probability']) == -1 and float(detection['iou']) == 0.0:
                FN += 1
            elif float(detection['iou']) < 0.6:
                FN += 1
            else:
                TP += 1

    P = TP + FN
    N = TN + FP
    TPR = TP / P

    TNR = TN / N
    PPV = TP / (TP + FP)
    NPV = TN / (TN + FN)
    FNR = FN / P

    FPR = FP / N

    FDR = FP / (FP + TP)
    FOR = FN / (FN + TN)

    ACC = (TP + TN) / (P + N)

    F1 = 2 * ((PPV * TPR) / (PPV + TPR))
    MCC = ((TP * TN - FP * FN) / sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)))

    FPPI = FP / image_counter

    if print_results:
        print(os.path.join(args.input, filename) + "," + str(int(TP)) + "," + str(int(TN)) + "," + str(int(FP)) + "," +
              str(int(FN)) + "," + str(TPR) + "," + str(FNR) + "," + str(FPPI) + "," + str(ACC))
        # print("Thresh: " + str(thresh))
        # print("True Positive (TP): " + str(int(TP)))
        # print("True Negative (TN): " + str(int(TN)))
        # print("False Positive (FP): " + str(int(FP)))
        # print("False Negative (FN): " + str(int(FN)))
        # print("")
        # print("sensitivity, recall, hit rate, or true positive rate (TPR): " + str(TPR))
        # # print("specificity, selectivity or true negative rate (TNR): " + str(TNR))
        # # print("precision or positive predictive value (PPV): " + str(PPV))
        # # # print("negative predictive value (NPV): " + str(NPV))
        # print("miss rate or false negative rate (FNR): " + str(FNR))
        # # print("fall-out or false positive rate (FPR): " + str(FPR))
        # # print("false discovery rate (FDR): " + str(FDR))
        # # # print("false omission rate (FOR): " + str(FOR))
        # # print("")
        # print("accuracy (ACC): " + str(ACC))
        # # print("F1 score: " + str(F1))
        # # # print("Matthews correlation coefficient (MCC): " + str(MCC))
        # print("false positive per image or FPPI: " + str(FPPI))
        # print("")

    if args.validate:
        print_results = False

    graph_values['FNR'].append(FNR)
    graph_values['TPR'].append(TPR)
    graph_values['FPPI'].append(FPPI)

# graph_values['FPR'].append(0.0)
# graph_values['TPR'].append(0.0)

# output to static HTML file
output_file(args.output + basename)

source = ColumnDataSource(data=graph_values)

# p = figure(plot_width=400, plot_height=400, tooltips=TOOLTIPS,
#            title="Mouse over the dots")

# create a new plot
p = figure(
    title="ROC curve",
    active_scroll="wheel_zoom",
    x_axis_label='False Positive Per Image', y_axis_label='True Positive Rate, False Negative Rate',
    sizing_mode='stretch_both',
    # x_range=(0, 1), y_range=(0, 1)
)
x_center = [0, 1]
y_center = [0, 1]

# add some renderers
p.line(x='FPPI', y='TPR', legend="True Positive Rate [FPPI:TPR]", color="blue", line_width=1.5, source=source)
p.circle(x='FPPI', y='TPR', legend="True Positive Rate [FPPI:TPR]", fill_color="blue", line_color="blue", size=4, source=source)
p.line(x='FPPI', y='FNR', legend="False Negative Rate [FPPI:FNR]", color="red", line_width=1.5, source=source)
p.square(x='FPPI', y='FNR', legend="False Negative Rate [FPPI:FNR]", fill_color="red", line_color="red", size=4, source=source)
# p.line(x_center, y_center, line_color='brown', line_dash='4 4')

if args.show:
    # save and show the results
    show(p)
else:
    # save the results
    save(p)
