#
#   Author: Tomas Libal
#   Date:   27.4.2019
#
#   This program will show histogram based on sizes of licence plates from training dataset
#

import os
from PIL import Image

from bokeh.io import output_file, show
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, SingleIntervalTicker, LinearAxis

from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

parser = ArgumentParser(description="This script will show histogram based on sizes of LPs from training dataset.",
                        epilog="Author: Tomas Libal", formatter_class=RawTextHelpFormatter)

parser.add_argument("-i", "--in", help="Path to input txt file", action="store", dest="input", required=True)
parser.add_argument("-o", "--out", help="Path to output directory, in which will be histogram saved",
                    action="store", dest="output", default="/home/xlibal00/Documents/BP/roc_graphs/")

args = parser.parse_args()

if not args.output.endswith('/'):
    args.output = args.output + "/"

count_of_sizes = [0]*758
LP_counter = 0


# get image properties
for image in open(args.input, "r").readlines():
    txt_path = os.path.splitext(image)[0].replace("//", "/") + ".txt"
    width, height = Image.open(image.rstrip('\n')).size

    # get bbox properties
    for line in open(txt_path, "r").readlines():
        coords = line.split()
        del coords[0]

        left = int((float(coords[0]) - float(coords[2]) / 2.0) * width)
        right = int((float(coords[0]) + float(coords[2]) / 2.0) * width)
        top = int((float(coords[1]) - float(coords[3]) / 2.0) * height)
        bottom = int((float(coords[1]) + float(coords[3]) / 2.0) * height)

        if left < 0:
            left = 0
        if right > width - 1:
            right = width - 1
        if top < 0:
            top = 0
        if bottom > height - 1:
            bottom = height - 1

        LP_counter += 1
        print(str(LP_counter) + ": " + image.rstrip('\n') + "\t" + str(right-left))
        count_of_sizes[right-left] += 1

# gather histogram data
split = 25
histogram_data = {'value': [], 'left': [], 'right': []}
l_interval, r_interval = 0, split
total = 0
total_total = 0
for position, size in zip(range(1, len(count_of_sizes)), count_of_sizes):
    print(position)
    print(size)
    print()
    total_total += size
    if position % split == 0 and l_interval < 200:
        total += size
        histogram_data['value'].append(total)
        histogram_data['left'].append(l_interval)
        histogram_data['right'].append(r_interval)
        l_interval += split
        r_interval += split
        total = 0
    else:
        total += size

histogram_data['value'].append(total)
histogram_data['left'].append(l_interval)
histogram_data['right'].append(r_interval)

print("total:" + str(total_total))

# create graph
# output to static HTML file
output_file(args.output + "histogram.html", title="histogram.html")

# create a new plot
p = figure(title="Histogram velikostí registračních značek",
           active_scroll="wheel_zoom",
           x_axis_label='Šířka registrační značky (px)', y_axis_label='Počet registračních značek',
           sizing_mode='stretch_both'

           )
ticker = SingleIntervalTicker(interval=25, num_minor_ticks=9)
xaxis = LinearAxis(ticker=ticker)
p.add_layout(xaxis, 'below')

p.output_backend = "svg"

source = ColumnDataSource(data=histogram_data)
p.quad(bottom=0, top='value', left='left', right='right',
       fill_color='deepskyblue', line_color='black', source=source)

# save and show the results
show(p)



