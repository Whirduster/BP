import os
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter
from random import randint

parser = ArgumentParser(description="Creator of test.txt, train.txt and validate.txt files ",
                        epilog="Author: Tomas Libal",
                        formatter_class=RawTextHelpFormatter)

parser.add_argument("-i", "--input", help="Input train file", action="store", dest="input", required=True)
parser.add_argument("-o", "--output", help="Output file train file", action="store", dest="output", required=True)
parser.add_argument("-d", "--delete", help="Delete files if exists", action="store_true", dest='delete')
parser.add_argument("-n", "--number", help="Number of images", action="store", dest='number', type=int, required=True)

args = parser.parse_args()

if args.output is None:
    args.output = os.path.dirname(os.path.abspath(__file__))

if os.path.exists(args.output) and args.delete:
    os.remove(args.output)

train_txt = open(args.output, "a")

with open(args.input, "r") as input_file:
    lines = input_file.readlines()
    input_file.close()

output_list = []
for i in range(0, args.number):
    random_value = randint(0, len(lines) - 1)
    if random_value > len(lines):
        print("List overflow")
        exit()
    else:
        output_list.append(lines[random_value])
        del lines[random_value]

output_list.sort()
for line in output_list:
    train_txt.write(line)
train_txt.close()
