#
#   Author: Tomas Libal
#   Date:   3.2.2019
#
#   This script will create dataset of images from video file on input
#

import sys
import os
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

# Program To Read video 
# and Extract Frames 
import cv2


# Function to extract frames 
def frame_capture(input_path, output_path, split):
    # Path to video file
    vid_obj = cv2.VideoCapture(input_path)

    # Used as counter variable
    count = 0

    # checks whether frames were extracted
    success = 1

    while success:

        # vid_obj object calls read
        # function extract frames
        success, image = vid_obj.read()
        if count % split == 0:
            # Saves the frames with frame-count
            print(os.path.join(output_path, os.path.splitext(os.path.basename(input_path))[0] + "_frame_%d.jpg" % count))
            cv2.imwrite(os.path.join(output_path, os.path.splitext(os.path.basename(input_path))[0] + "_frame_%d.jpg" % count), image)

        count += 1


# Driver Code
if __name__ == '__main__':

    parser = ArgumentParser(description="Split video into images", epilog="Author: Tomas Libal",
                            formatter_class=RawTextHelpFormatter)

    parser.add_argument("-i", help="Input video path", action="store", dest="input", required=True)
    parser.add_argument("-o", help="Output images path", action="store", dest="output", required=True)
    parser.add_argument("-f", help="Save each n-th frame", action="store", dest="frame")
    args = parser.parse_args()

    if not os.path.exists(args.input):
        print("Input video does not exist")
        exit(1)
    elif not os.path.exists(args.output):
        os.makedirs(args.output)

    if args.frame:
        frame = args.frame
    else:
        frame = 1
    frame_capture(args.input, args.output, int(frame))
