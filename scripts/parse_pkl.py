#
#   Author: Tomas Libal
#   Date:   16.12.2018
#
#   This script will parse input .pkl files in ZoomLPDataset_sorted dataset
#

import pickle
import os
import sys
from PIL import Image
from random import randint


def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return x, y, w, h


path = dir_name = pkl_filename = ""
pkl_files = []
train = False

# argument parsing
if len(sys.argv) == 2:
    if sys.argv[1] == "-h" or sys.argv[1] == "--help":
        print("Type the path to the .pkl file\n")
        print(" /pkl/file/path   - path to pkl file. Parse this pkl file.\n")
        print(" /pkl/file/path -t   - create test and train files for this pkl file\n")
        print(" /pkl/dir/path -a  - create test and train files for all pkl files in given directory\n")
        exit(0)
    else:
        path = sys.argv[1]
        dir_name = os.path.dirname(path)
        pkl_files.append(os.path.join(dir_name, os.path.basename(path)))
elif len(sys.argv) == 3:
    path = sys.argv[1]
    dir_name = os.path.dirname(path)
    if sys.argv[2] == "-t" or sys.argv[2] == "--train":
        train = True
        pkl_files.append(os.path.join(dir_name, os.path.basename(path)))
    elif sys.argv[2] == "-a" or sys.argv[2] == "--all":
        train = True
        for file in os.listdir(dir_name):
            if file.endswith(".pkl"):
                pkl_files.append(os.path.join(dir_name, file))
        pkl_files.sort()
else:
    print("Please type the path to the .pkl file\nUse -h or --help\n")
    print("Use: path/to/pkl/file [-t]")
    exit(0)

# divide image paths into 3 datasets
if train:
    train_txt = open(os.path.join(dir_name, "train.txt"), "w+")
    test_txt = open(os.path.join(dir_name, "test.txt"), "w+")
    validate_txt = open(os.path.join(dir_name, pkl_filename + "validate.txt"), "w+")

for pkl_path in pkl_files:
    with open(pkl_path, 'rb') as pkl:
        data = pickle.load(pkl)
        pkl.close()
    path = pkl_path
    dir_name = os.path.dirname(path)
    pkl_filename = os.path.splitext(os.path.basename(path))[0]

    with open(os.path.join(dir_name, pkl_filename + "_list.txt"), "w+") as all_txt:
        for frame in data['detections']:
            frame_id = frame['frame_id']
            file_name = 'im_' + '%06d' % frame_id
            print(dir_name + '/' + pkl_filename + '/' + file_name + ".txt")
            with open(os.path.join(dir_name, pkl_filename, file_name + ".txt"), "w+") as yolo_txt:
                for plate in frame['lps']:

                    # Convert the data to YOLO format
                    xmin = plate[5][0][0]
                    xmax = plate[5][2][0]
                    ymin = plate[5][0][1]
                    ymax = plate[5][2][1]

                    img_path = os.path.join(dir_name, pkl_filename, file_name + ".jpg")
                    im = Image.open(img_path)
                    w = int(im.size[0])
                    h = int(im.size[1])
                    print(w, h)
                    print(float(xmin), float(xmax), float(ymin), float(ymax))
                    b = (float(xmin), float(xmax), float(ymin), float(ymax))
                    bb = convert((w, h), b)
                    print(bb)
                    yolo_txt.write(str('0' + " " + " ".join([str(a) for a in bb]) + '\n'))

                # Save those images with bb into list
                all_txt.write(os.path.join(dir_name, pkl_filename, file_name + ".jpg\n"))
                if train:
                    random_value = randint(0, 99)
                    if random_value < 10:
                        test_txt.write(os.path.join(dir_name, pkl_filename, file_name + ".jpg\n"))
                    elif random_value < 20:
                        validate_txt.write(os.path.join(dir_name, pkl_filename, file_name + ".jpg\n"))
                    else:
                        train_txt.write(os.path.join(dir_name, pkl_filename, file_name + ".jpg\n"))

                yolo_txt.close()
        all_txt.close()
if train:
    train_txt.close()
    test_txt.close()
    validate_txt.close()
