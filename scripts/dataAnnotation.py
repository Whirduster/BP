import os
import sys
from PIL import Image


root = ""

if len(sys.argv) == 2:
    if sys.argv[1] == "-h" or sys.argv[1] == "--help":
        print("Type the path to the directory\nIn case of error try: pip install Pillow")
        exit(0)
    else:
        #root = sys.argv[1]
        root = os.path.abspath(sys.argv[1])
else:
    print("Please type the path to the directory\nUse -h or --help\n")
    exit(0)

try:
    for subdir, dirs, files in os.walk(root):
        for image in files:
            filename, extension = os.path.splitext(image)
            if extension == ".jpg":
                path = os.path.join(subdir, image)
                print(path)
                im = Image.open(path)
                image_width, image_height = im.size
                absolute_x = image_width/2  # (center position of object from image)
                absolute_y = image_height/2  # (center y position of object from image)
                absolute_height = image_height  # (original height of object from image)
                absolute_width = image_width  # (original width of object from image)
                object_class = 0
                x = absolute_x / image_width
                y = absolute_y / image_height
                width = absolute_width / image_width
                height = absolute_height / image_height
                txt = open(os.path.join(subdir, filename) + ".txt", "w+")
                txt.write("%d " % object_class)
                txt.write("%f " % x)
                txt.write("%f " % y)
                txt.write("%f " % width)
                txt.write("%f" % height)
                txt.close()
            else:
                continue
except FileNotFoundError:
    sys.stderr.write("ERROR: File not found\n")
    exit(10)


