

# Detection of Vehicle License Plates in Video
**Bachelor Thesis**  
**Author: Tomáš Líbal**  
**Supervisor: prof. Ing. Adam Herout, Ph.D.**  
**Brno, 2019**  

## Requirements
 -  **Ubuntu 16.04**
 -  **OpenCV 3.4.2**
	 - https://github.com/opencv/opencv/releases/tag/3.4.2
 - **CUDA 9.0**
	- https://developer.nvidia.com/cuda-90-download-archive
 - **cuDNN Runtime+Developer for CUDA 9.0**
	- https://developer.nvidia.com/rdp/cudnn-archive
 - **Anotation tool Yolo_mark**
	- https://github.com/AlexeyAB/Yolo_mark
 - **Python 2.7 or Python 3.5**

## Subfolders
Please follow also other README files in subfolders

### scripts folder
- The scripts folder contains Python scripts that process various data or plot graphs.
- The description of each script's function is written in its header.
- All scripts are written for Python 2.7 and Python 3.5
- Help for each script can be called with the -h argument
    ```sh
    $ python [script_name.py] -h
    $ python3.5 [script_name.py] -h
    ```
- Each help contains descriptions of each argument
- For drawing curves it is necessary to download the library Bokeh
    ```sh
    pip install bokeh
    ```
### darknet folder
- The darknet folder contains the taken and modified [Darknet](https://github.com/pjreddie/darknet) source codes
- In the header of each modified script, both the original author and the author of the modifications, a list of modified functions, and a list of added features are named
- All changes compared to the original code are marked with comments

### thesis folder
- This folder contains all the files needed to compile and create electronic version of technical report of the bachelor thesis
- There is also poster and presentation video which represent the results of the work


