![Darknet Logo](http://pjreddie.com/media/files/darknet-black-small.png)

# Darknet #
Darknet is an open source neural network framework written in C and CUDA. It is fast, easy to install, and supports CPU and GPU computation.

For more information see the [Darknet project website](http://pjreddie.com/darknet).

For questions or issues please use the [Google Group](https://groups.google.com/forum/#!forum/darknet).

## Requirements
- **Download YOLOv3-tiny model**
	- https://github.com/pjreddie/darknet/blob/master/cfg/yolov3-tiny.cfg
- In **Makefile** change:
	- `GPU=1` to accelerate by using GPU
	- `CUDNN=1` to accelerate training by using GPU
	- `OPENCV=1` to allow to detect on video files and video streams
	- **Change number of filtres**
		-   On `line 127` set  `filters=18`
		-   On `line 135` set `classes=1`
		-   On `line 171` set `filters=18`
		-   On `line 177` set `classes=1`
	- **Add `-gencode arch=compute_61,code=[sm_61,compute_61]`** at the end of `ARCH`
		- this will support NVIDIA GeForce GTX 10 series GPUs
- **Download darknet53.conv.74 pretrained weights**
	- https://pjreddie.com/media/files/darknet53.conv.74
## Training of YOLOv3-tiny model
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector train -weights_name WEIGHTS_NAME -cfg CFG_FILE -weights WEIGHT_FILE
			-graph_dir GRAPH_DIRECTORY -train TRAIN_FILE -backup BACKUP_DIR
```
#### compulsory arguments:
```
  -weights_name WEIGHTS_NAME	Prefix of output weight file names
  -cfg CFG_FILE   		Path to configuration file
  -weights WEIGHT_FILE    	Path to initial .weights file
  -graph_dir GRAPH_DIRECTORY	Path to the output file in which will be training data saved
  -train TRAIN_FILE		Path to file with image paths
  -backup BACKUP_DIR		Path to the output directory for weights storage
```
## Testing of YOLOv3-tiny model
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector test [-data DATA_FILE] -cfg CFG_FILE -weights WEIGHT_FILE 
			[-iou] [-probability] [-thresh THRESH] [-filename IMAGE]
```
#### compulsory arguments:
```
  -cfg CFG_FILE   		Path to configuration file
  -weights WEIGHT_FILE    	Path to .weights file
```
#### mandatory arguments:
```
  -data DATA_FILE	Path to data file (default cfg/data/universal.data)
  -iou			If this argument is used, only IoU results are returned
  -probability		If this argument is used, only probabilities are returned
  -thresh THRESH	Lower treshold value
  -filename IMAGE	The path to the image to be tested
```
## Validation of YOLOv3-tiny model
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector valid [-data DATA_FILE] -cfg CFG_FILE -weights WEIGHT_FILE
			[-prefix OUTPUT_DIR] -out OUTPUT_FILE_NAME 
			-valid FILE_WITH_VALIDATE_IMAGES
```
#### compulsory arguments:
```
  -cfg CFG_FILE   			Path to configuration file
  -weights WEIGHT_FILE    		Path to .weights file
  -out OUTPUT_FILE_NAME			Name output file in which will be validation results saved
  -valid FILE_WITH_VALIDATE_IMAGES	Path to file with paths to validation images
```
#### mandatory arguments:
```
  -data DATA_FILE	Path to data file (default cfg/data/universal.data)
  -prefix OUTPUT_DIR	Path to output directory in which will be file with validation results saved
```
## Running video detections
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector demo [-data DATA_FILE] -cfg CFG_FILE -weights WEIGHT_FILE
			[-thresh THRESH] [-filename VIDEO]
```
#### compulsory arguments:
```
  -cfg CFG_FILE   		Path to configuration file
  -weights WEIGHT_FILE    	Path to .weights file
```
#### mandatory arguments:
```
  -data DATA_FILE	Path to data file (default cfg/data/universal.data)
  -thresh THRESH	Lower treshold value
  -filename VIDEO 	Path to input video file (default webcam)
```
## Exporting video detections
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector export [-data DATA_FILE] -cfg CFG_FILE -weights WEIGHT_FILE
			-prefix OUTPUT_DIR [-thresh THRESH] [-filename VIDEO]
			 [-export_image] [-export_video] [-show] [-fps FPS]
	 
	 -prefix must be full path to OUTPUT_DIR, also after OUTPUT_DIR can be OUTPUT_FILE_NAME prefix
```
#### compulsory arguments:
```
  -cfg CFG_FILE   		Path to configuration file
  -weights WEIGHT_FILE    	Path to .weights file
  -prefix OUTPUT_DIR		Path to directory in which will be image saved and image name prefix
```
#### mandatory arguments:
```
  -data DATA_FILE	Path to data file (default cfg/data/universal.data)
  -thresh THRESH	Lower treshold value
  -filename VIDEO 	Path to input video file (default webcam)
  -export_image		If this argument is used, each frame will be saved individually as image
  -export_video		If this argument is used, final video with marked detections will be saved
  -show			If this argument is used, render the video into a window on the screen
  -fps FPS		Frame rate of final video (default: input video FPS)
```
## Save video detections
```sh
$ [cd darknet]
$ [make]
$ ./darknet detector save_detections [-data DATA_FILE] -cfg CFG_FILE 
				 -weights WEIGHT_FILE -prefix OUTPUT_DIR 
				 [-thresh THRESH] [-filename] [-show] 
				 [-fps FPS] [-skip FRAME_SKIP] [-save_original]
				 
	-prefix must be full path to OUTPUT_DIR, also after OUTPUT_DIR can be OUTPUT_FILE_NAME prefix
```
#### compulsory arguments:
```
  -cfg CFG_FILE   		Path to configuration file
  -weights WEIGHT_FILE    	Path to .weights file
  -prefix OUTPUT_DIR		Path to directory in which will be image saved and image name prefix
```
#### mandatory arguments:
```
  -data DATA_FILE	Path to data file (default cfg/data/universal.data)
  -thresh THRESH	Lower treshold value
  -filename VIDEO 	Path to input video file (default webcam)
  -show			If this argument is used, render the video into a window on the screen
  -fps FPS		Frame rate of final video (default: input video FPS)
  -skip FRAME_SKIP	Number of frames to be skipped (default: 1)
  -save_original	If this argument is used, only frames with detected licence plates will be saved, but the bounding box in the image will not be rendered
*/
```

