/**********************************************************************
*
*  Taken and modified code
*  
*  Original author: Joseph Redmon
*  Author of modifications: Tomas Libal
*
*  Modified parts of functions are marked by comments
*  Some parts of the code were removed because they were unnecessary
*
*  Modified functions: 
*       train_detector()
*       print_cocos()
*       validate_detector()
*       test_detector()
*       run_detector()
*
*  Added functions:
*        md_comparator()
*
**********************************************************************/

#include "darknet.h"
#include <ctype.h>     // added
#define MAX(x, y) ((x > y) ? x : y)   // Macro to determine if more than one object is detected in the image

/***
 * Modified training function for neuronal network
 *
 * Printing training values to file has been added and saving .weights has been changed to 10000 iterations
 *
 * @param weights_file_name Prefix of output weight file names
 * @param cfgfile Path to configuration file
 * @param weightfile Path to initial .weights file
 * @param gpus GPU indexes
 * @param ngpus Number of GPU
 * @param clear Not sure what it is for - leave as it is
 * @param graph_name Path to the output file in which will be training data saved
 * @param train_images Path to file with image paths
 * @param backup_directory Path to the output directory for weights storage
*/
void train_detector(char *weights_file_name, char *cfgfile, char *weightfile, int *gpus, int ngpus, int clear, char graph_name[], char *train_images, char *backup_directory)
{
    // removing data retrieval from a file, instead of being taken from arguments

    srand(time(0));
    char *base = basecfg(cfgfile);
    printf("%s\n", base);
    float avg_loss = -1;
    network **nets = calloc(ngpus, sizeof(network));

    srand(time(0));
    int seed = rand();
    int i;
    for(i = 0; i < ngpus; ++i){
        srand(seed);
#ifdef GPU
        cuda_set_device(gpus[i]);
#endif
        nets[i] = load_network(cfgfile, weightfile, clear);
        nets[i]->learning_rate *= ngpus;
    }
    srand(time(0));
    network *net = nets[0];

    int imgs = net->batch * net->subdivisions * ngpus;
    printf("Learning Rate: %g, Momentum: %g, Decay: %g\n", net->learning_rate, net->momentum, net->decay);
    data train, buffer;

    layer l = net->layers[net->n - 1];

    int classes = l.classes;
    float jitter = l.jitter;

    list *plist = get_paths(train_images);
    //int N = plist->size;
    char **paths = (char **)list_to_array(plist);

    load_args args = get_base_args(net);
    args.coords = l.coords;
    args.paths = paths;
    args.n = imgs;
    args.m = plist->size;
    args.classes = classes;
    args.jitter = jitter;
    args.num_boxes = l.max_boxes;
    args.d = &buffer;
    args.type = DETECTION_DATA;
    //args.type = INSTANCE_DATA;
    args.threads = 64;

    pthread_t load_thread = load_data(args);
    double time;
    int count = 0;
    //while(i*imgs < N*120){
    while(get_current_batch(net) < net->max_batches){
        if(l.random && count++%10 == 0){
            printf("Resizing\n");
            int dim = (rand() % 10 + 10) * 32;
            if (get_current_batch(net)+200 > net->max_batches) dim = 608;
            //int dim = (rand() % 4 + 16) * 32;
            printf("%d\n", dim);
            args.w = dim;
            args.h = dim;

            pthread_join(load_thread, 0);
            train = buffer;
            free_data(train);
            load_thread = load_data(args);

            #pragma omp parallel for
            for(i = 0; i < ngpus; ++i){
                resize_network(nets[i], dim, dim);
            }
            net = nets[0];
        }
        time=what_time_is_it_now();
        pthread_join(load_thread, 0);
        train = buffer;
        load_thread = load_data(args);

        printf("Loaded: %lf seconds\n", what_time_is_it_now()-time);

        time=what_time_is_it_now();
        float loss = 0;
#ifdef GPU
        if(ngpus == 1){
            loss = train_network(net, train);
        } else {
            loss = train_networks(nets, ngpus, train, 4);
        }
#else
        loss = train_network(net, train);
#endif
        if (avg_loss < 0) avg_loss = loss;
        avg_loss = avg_loss*.9 + loss*.1;

        i = get_current_batch(net);
        printf("%ld: %f, %f avg, %f rate, %lf seconds, %d images\n", get_current_batch(net), loss, avg_loss, get_current_rate(net), what_time_is_it_now()-time, i*imgs);
    
/************************** Printing training values to file **************************/

    FILE *graph = fopen(graph_name, "a");

    if (graph == NULL) {
        fprintf(stderr, "Error opening file for graph!\n");
        exit(1);
    }

    fprintf(graph, "%ld %f %f\n", get_current_batch(net), loss, avg_loss);
    fclose(graph);  
/**************************************************************************************/

        if(i%100==0){
#ifdef GPU
            if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
            char buff[256];
            sprintf(buff, "%s/%s.backup", backup_directory, base);
            save_weights(net, buff);
        }
        if(i%1000==0 || (i < 1000 && i%200 == 0)){      // changed saving weights - each 1000 iteration instead of 10 000
#ifdef GPU
            if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
            char buff[256];
            sprintf(buff, "%s/%s_%d.weights", backup_directory, weights_file_name, i);   // changed base to weights_file_name
            save_weights(net, buff);
        }
        free_data(train);
    }
#ifdef GPU
    if(ngpus != 1) sync_nets(nets, ngpus, 0);
#endif
    char buff[256];
    sprintf(buff, "%s/%s_final.weights", backup_directory, base);
    save_weights(net, buff);
}

/***
 * Function for inserting data into json file
 *
 * Additional required information is written to the json file
 *
 * @param fp Pointer to opened json file
 * @param image_path Path to image which is beeing processed
 * @param dets Detections that were obtained in image
 * @param classes Number of classes
 * @param w Width of the image
 * @param h Height of the image
*/
static void print_cocos(FILE *fp, char *image_path, detection *dets, int num_boxes, int classes, int w, int h)
{   
    fprintf(fp, "{\"image_path\":\"%s\", \"image_width\": %d, \"image_height\": %d, \"detections\":[\n", image_path, w, h);
    int i, j, detected = 0;
    //int image_id = get_coco_image_id(image_path);
    for(i = 0; i < num_boxes; ++i){
        float xmin = dets[i].bbox.x - dets[i].bbox.w/2.;
        float xmax = dets[i].bbox.x + dets[i].bbox.w/2.;
        float ymin = dets[i].bbox.y - dets[i].bbox.h/2.;
        float ymax = dets[i].bbox.y + dets[i].bbox.h/2.;

        if (xmin < 0) xmin = 0;
        if (ymin < 0) ymin = 0;
        if (xmax > w) xmax = w;
        if (ymax > h) ymax = h;

        for(j = 0; j < classes; ++j){
            if (dets[i].prob[j]) {
                fprintf(fp, "{\"yolo_cords\":[%f, %f, %f, %f], \"left\":%f, \"right\":%f, \"top\": %f, \"bottom\":%f, \"score\":%f, \"iou\":0, \"used\":false, \"val_bbox_width\":0},\n", 
                    dets[i].bbox.x, dets[i].bbox.y, dets[i].bbox.w, dets[i].bbox.h, xmin, xmax, ymin, ymax, dets[i].prob[j]);
                detected = 1;
            }
        }
    }
    if(detected) fseek(fp, -2, SEEK_CUR); 
    fprintf(fp, "]},\n");
}

void print_detector_detections(FILE **fps, char *id, detection *dets, int total, int classes, int w, int h)
{
    int i, j;
    for(i = 0; i < total; ++i){
        float xmin = dets[i].bbox.x - dets[i].bbox.w/2. + 1;
        float xmax = dets[i].bbox.x + dets[i].bbox.w/2. + 1;
        float ymin = dets[i].bbox.y - dets[i].bbox.h/2. + 1;
        float ymax = dets[i].bbox.y + dets[i].bbox.h/2. + 1;

        if (xmin < 1) xmin = 1;
        if (ymin < 1) ymin = 1;
        if (xmax > w) xmax = w;
        if (ymax > h) ymax = h;

        for(j = 0; j < classes; ++j){
            if (dets[i].prob[j]) fprintf(fps[j], "%s %f %f %f %f %f\n", id, dets[i].prob[j],
                    xmin, ymin, xmax, ymax);
        }
    }
}

void print_imagenet_detections(FILE *fp, int id, detection *dets, int total, int classes, int w, int h)
{
    int i, j;
    for(i = 0; i < total; ++i){
        float xmin = dets[i].bbox.x - dets[i].bbox.w/2.;
        float xmax = dets[i].bbox.x + dets[i].bbox.w/2.;
        float ymin = dets[i].bbox.y - dets[i].bbox.h/2.;
        float ymax = dets[i].bbox.y + dets[i].bbox.h/2.;

        if (xmin < 0) xmin = 0;
        if (ymin < 0) ymin = 0;
        if (xmax > w) xmax = w;
        if (ymax > h) ymax = h;

        for(j = 0; j < classes; ++j){
            int class = j;
            if (dets[i].prob[class]) fprintf(fp, "%d %d %f %f %f %f %f\n", id, j+1, dets[i].prob[class],
                    xmin, ymin, xmax, ymax);
        }
    }
}

void validate_detector_flip(char *datacfg, char *cfgfile, char *weightfile, char *outfile)
{
    int j;
    list *options = read_data_cfg(datacfg);
    char *valid_images = option_find_str(options, "valid", "data/train.list");
    char *name_list = option_find_str(options, "names", "data/names.list");
    char *prefix = option_find_str(options, "results", "results");
    char **names = get_labels(name_list);
    char *mapf = option_find_str(options, "map", 0);
    int *map = 0;
    if (mapf) map = read_map(mapf);

    network *net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 2);
    fprintf(stderr, "Learning Rate: %g, Momentum: %g, Decay: %g\n", net->learning_rate, net->momentum, net->decay);
    srand(time(0));

    list *plist = get_paths(valid_images);
    char **paths = (char **)list_to_array(plist);

    layer l = net->layers[net->n-1];
    int classes = l.classes;

    char buff[1024];
    char *type = option_find_str(options, "eval", "voc");
    FILE *fp = 0;
    FILE **fps = 0;
    int coco = 0;
    int imagenet = 0;
    if(0==strcmp(type, "coco")){
        if(!outfile) outfile = "coco_results";
        snprintf(buff, 1024, "%s/%s.json", prefix, outfile);
        fp = fopen(buff, "w");
        fprintf(fp, "[\n");
        coco = 1;
    } else if(0==strcmp(type, "imagenet")){
        if(!outfile) outfile = "imagenet-detection";
        snprintf(buff, 1024, "%s/%s.txt", prefix, outfile);
        fp = fopen(buff, "w");
        imagenet = 1;
        classes = 200;
    } else {
        if(!outfile) outfile = "comp4_det_test_";
        fps = calloc(classes, sizeof(FILE *));
        for(j = 0; j < classes; ++j){
            snprintf(buff, 1024, "%s/%s%s.txt", prefix, outfile, names[j]);
            fps[j] = fopen(buff, "w");
        }
    }

    int m = plist->size;
    int i=0;
    int t;

    float thresh = .005;
    float nms = .45;

    int nthreads = 4;
    image *val = calloc(nthreads, sizeof(image));
    image *val_resized = calloc(nthreads, sizeof(image));
    image *buf = calloc(nthreads, sizeof(image));
    image *buf_resized = calloc(nthreads, sizeof(image));
    pthread_t *thr = calloc(nthreads, sizeof(pthread_t));

    image input = make_image(net->w, net->h, net->c*2);

    load_args args = {0};
    args.w = net->w;
    args.h = net->h;
    //args.type = IMAGE_DATA;
    args.type = LETTERBOX_DATA;

    for(t = 0; t < nthreads; ++t){
        args.path = paths[i+t];
        args.im = &buf[t];
        args.resized = &buf_resized[t];
        thr[t] = load_data_in_thread(args);
    }
    double start = what_time_is_it_now();
    for(i = nthreads; i < m+nthreads; i += nthreads){
        fprintf(stderr, "%d\n", i);
        for(t = 0; t < nthreads && i+t-nthreads < m; ++t){
            pthread_join(thr[t], 0);
            val[t] = buf[t];
            val_resized[t] = buf_resized[t];
        }
        for(t = 0; t < nthreads && i+t < m; ++t){
            args.path = paths[i+t];
            args.im = &buf[t];
            args.resized = &buf_resized[t];
            thr[t] = load_data_in_thread(args);
        }
        for(t = 0; t < nthreads && i+t-nthreads < m; ++t){
            char *path = paths[i+t-nthreads];
            char *id = basecfg(path);
            copy_cpu(net->w*net->h*net->c, val_resized[t].data, 1, input.data, 1);
            flip_image(val_resized[t]);
            copy_cpu(net->w*net->h*net->c, val_resized[t].data, 1, input.data + net->w*net->h*net->c, 1);

            network_predict(net, input.data);
            int w = val[t].w;
            int h = val[t].h;
            int num = 0;
            detection *dets = get_network_boxes(net, w, h, thresh, .5, map, 0, &num);
            if (nms) do_nms_sort(dets, num, classes, nms);
            if (coco){
                print_cocos(fp, path, dets, num, classes, w, h);
            } else if (imagenet){
                print_imagenet_detections(fp, i+t-nthreads+1, dets, num, classes, w, h);
            } else {
                print_detector_detections(fps, id, dets, num, classes, w, h);
            }
            free_detections(dets, num);
            free(id);
            free_image(val[t]);
            free_image(val_resized[t]);
        }
    }
    for(j = 0; j < classes; ++j){
        if(fps) fclose(fps[j]);
    }
    if(coco){
        fseek(fp, -2, SEEK_CUR); 
        fprintf(fp, "\n]\n");
        fclose(fp);
    }
    fprintf(stderr, "Total Detection Time: %f Seconds\n", what_time_is_it_now() - start);
}


/***
 * Modified valudation function for neuronal network
 *
 * There was only some little changes in working with .data file
 *
 * @param datacfg Path to data file
 * @param cfgfile Path to configuration file
 * @param weightfile Path to .weights file
 * @param prefix Path to output directory in which will be file with validation results saved
 * @param outfile Name output file in which will be validation results saved
 * @param valid_images Path to file with paths to validation images
*/
void validate_detector(char *datacfg, char *cfgfile, char *weightfile, char *prefix, char *outfile, char *valid_images)
{

    // removing data retrieval from a file, instead of being taken from arguments

    int j;
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);
    char *mapf = option_find_str(options, "map", 0);
    int *map = 0;
    if (mapf) map = read_map(mapf);

    network *net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 1);
    fprintf(stderr, "Learning Rate: %g, Momentum: %g, Decay: %g\n", net->learning_rate, net->momentum, net->decay);
    srand(time(0));

    list *plist = get_paths(valid_images);
    char **paths = (char **)list_to_array(plist);

    layer l = net->layers[net->n-1];
    int classes = l.classes;

    char buff[1024];
    char *type = option_find_str(options, "eval", "voc");
    FILE *fp = 0;
    FILE **fps = 0;
    int coco = 0;
    int imagenet = 0;
    if(0==strcmp(type, "coco")){
        if(!outfile) outfile = "coco_results";
        snprintf(buff, 1024, "%s/%s.json", prefix, outfile);
        fp = fopen(buff, "w");
        fprintf(fp, "[\n");
        coco = 1;
    } else if(0==strcmp(type, "imagenet")){
        if(!outfile) outfile = "imagenet-detection";
        snprintf(buff, 1024, "%s/%s.txt", prefix, outfile);
        fp = fopen(buff, "w");
        imagenet = 1;
        classes = 200;
    } else {
        if(!outfile) outfile = "comp4_det_test_";
        fps = calloc(classes, sizeof(FILE *));
        for(j = 0; j < classes; ++j){
            snprintf(buff, 1024, "%s/%s%s.txt", prefix, outfile, names[j]);
            fps[j] = fopen(buff, "w");
        }
    }


    int m = plist->size;
    int i=0;
    int t;

    float thresh = .005;
    float nms = .45;

    int nthreads = 4;
    image *val = calloc(nthreads, sizeof(image));
    image *val_resized = calloc(nthreads, sizeof(image));
    image *buf = calloc(nthreads, sizeof(image));
    image *buf_resized = calloc(nthreads, sizeof(image));
    pthread_t *thr = calloc(nthreads, sizeof(pthread_t));

    load_args args = {0};
    args.w = net->w;
    args.h = net->h;
    //args.type = IMAGE_DATA;
    args.type = LETTERBOX_DATA;

    for(t = 0; t < nthreads; ++t){
        args.path = paths[i+t];
        args.im = &buf[t];
        args.resized = &buf_resized[t];
        thr[t] = load_data_in_thread(args);
    }
    double start = what_time_is_it_now();
    for(i = nthreads; i < m+nthreads; i += nthreads){
        fprintf(stderr, "%d\n", i);
        for(t = 0; t < nthreads && i+t-nthreads < m; ++t){
            pthread_join(thr[t], 0);
            val[t] = buf[t];
            val_resized[t] = buf_resized[t];
        }
        for(t = 0; t < nthreads && i+t < m; ++t){
            args.path = paths[i+t];
            args.im = &buf[t];
            args.resized = &buf_resized[t];
            thr[t] = load_data_in_thread(args);
        }
        for(t = 0; t < nthreads && i+t-nthreads < m; ++t){
            char *path = paths[i+t-nthreads];
            char *id = basecfg(path);
            float *X = val_resized[t].data;
            network_predict(net, X);
            int w = val[t].w;
            int h = val[t].h;
            int nboxes = 0;
            detection *dets = get_network_boxes(net, w, h, thresh, .5, map, 1, &nboxes);
            if (nms) do_nms_sort(dets, nboxes, classes, nms);
            if (coco){
                print_cocos(fp, path, dets, nboxes, classes, w, h);
            } else if (imagenet){
                print_imagenet_detections(fp, i+t-nthreads+1, dets, nboxes, classes, w, h);
            } else {
                print_detector_detections(fps, id, dets, nboxes, classes, w, h);
            }
            free_detections(dets, nboxes);
            free(id);
            free_image(val[t]);
            free_image(val_resized[t]);
        }
    }
    for(j = 0; j < classes; ++j){
        if(fps) fclose(fps[j]);
    }
    if(coco){
        fseek(fp, -2, SEEK_CUR); 
        fprintf(fp, "\n]\n");
        fclose(fp);
    }
    fprintf(stderr, "Total Detection Time: %f Seconds\n", what_time_is_it_now() - start);
}

void validate_detector_recall(char *cfgfile, char *weightfile)
{
    network *net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 1);
    fprintf(stderr, "Learning Rate: %g, Momentum: %g, Decay: %g\n", net->learning_rate, net->momentum, net->decay);
    srand(time(0));

    list *plist = get_paths("data/coco_val_5k.list");
    char **paths = (char **)list_to_array(plist);

    layer l = net->layers[net->n-1];

    int j, k;

    int m = plist->size;
    int i=0;

    float thresh = .001;
    float iou_thresh = .5;
    float nms = .4;

    int total = 0;
    int correct = 0;
    int proposals = 0;
    float avg_iou = 0;

    for(i = 0; i < m; ++i){
        char *path = paths[i];
        image orig = load_image_color(path, 0, 0);
        image sized = resize_image(orig, net->w, net->h);
        char *id = basecfg(path);
        network_predict(net, sized.data);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, sized.w, sized.h, thresh, .5, 0, 1, &nboxes);
        if (nms) do_nms_obj(dets, nboxes, 1, nms);

        char labelpath[4096];
        find_replace(path, "images", "labels", labelpath);
        find_replace(labelpath, "JPEGImages", "labels", labelpath);
        find_replace(labelpath, ".jpg", ".txt", labelpath);
        find_replace(labelpath, ".JPEG", ".txt", labelpath);

        int num_labels = 0;
        box_label *truth = read_boxes(labelpath, &num_labels);
        for(k = 0; k < nboxes; ++k){
            if(dets[k].objectness > thresh){
                ++proposals;
            }
        }
        for (j = 0; j < num_labels; ++j) {
            ++total;
            box t = {truth[j].x, truth[j].y, truth[j].w, truth[j].h};
            float best_iou = 0;
            for(k = 0; k < l.w*l.h*l.n; ++k){
                float iou = box_iou(dets[k].bbox, t);
                if(dets[k].objectness > thresh && iou > best_iou){
                    best_iou = iou;
                }
            }
            avg_iou += best_iou;
            if(best_iou > iou_thresh){
                ++correct;
            }
        }

        fprintf(stderr, "%5d %5d %5d\tRPs/Img: %.2f\tIOU: %.2f%%\tRecall:%.2f%%\n", i, correct, total, (float)proposals/(i+1), avg_iou*100/total, 100.*correct/total);
        free(id);
        free_image(orig);
        free_image(sized);
    }
}

/***
 * Function for sorting the structure
 *
 * @param v1 First bounding-box
 * @param v2 Second bounding-box
 * @return Result of comparation
*/
int md_comparator(const void *v1, const void *v2) {
    const box *p1 = (box *)v1;
    const box *p2 = (box *)v2;
    if (p1->y < p2->y)
        return -1;
    else if (p1->y > p2->y)
        return +1;
    else if (p1->x < p2->x)
        return -1;
    else if (p1->x > p2->x)
        return +1;
    else
        return 0;
}

/***
* Function for testing the trained model
*
* This function is all modified and expanded
* Added rendering of IoU to result image and changed colors of bounding-boxes
* Test data printing, such as probabilities and IoU results, was also added
*
* @param datacfg Path to data file
* @param cfgfile Path to configuration file
* @param weightfile Path to .weights file
* @param filename The path to the image to be tested
* @param thresh Lower treshold
* @param hier_thresh Higher treshold
* @param outfile Path and name of output image
* @param fullscreen Result is rendered to fullscreen mode
* @param test_iou If this value is true, only IoU results are returned
* @param probability If this value is true, only probabilities are returned
*/
void test_detector(char *datacfg, char *cfgfile, char *weightfile, char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen, int test_iou, int probability)
{   
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);

    image **alphabet = load_alphabet();
    network *net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 1);
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    float nms=.45;
    int processing_cnt = 1;
    while(1){
        if(filename){
            strncpy(input, filename, 256);
        } else {
            if(!test_iou && !probability)
                printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if(!input) {
                fprintf(stderr, "\n");
                return;
            }
            strtok(input, "\n");

            fprintf(stderr, "\rProcessed %d images", processing_cnt);
            fflush(stderr);
            processing_cnt++;
        }
        image im = load_image_color(input,0,0);
        image sized = letterbox_image(im, net->w, net->h);
        layer l = net->layers[net->n-1];


        float *X = sized.data;
        time=what_time_is_it_now();
        network_predict(net, X);
        if(!test_iou && !probability) {
            printf("%s\nPredicted in %f seconds.\n", input, what_time_is_it_now()-time);
        } else {
            printf("\nImage path: %s\n", input);
        }
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        for (int i = 0; i < nboxes; ++i) dets[i].hit = 0;

        char * yolo_filename = input;

        //open txt file with same name as image
        char *end = yolo_filename + strlen(yolo_filename);
        while (end > yolo_filename && *end != '.') {
            --end;
        }

        if (end > yolo_filename) {
            *end = '\0';
        }

        strcat(yolo_filename, ".txt");


        // open txt file
        FILE *yolo_bb = fopen(input, "r");
        if (yolo_bb != NULL) {
            // get number of lines
            int lines, ch;
            lines = ch = 0;

            while(!feof(yolo_bb)) {
                ch = fgetc(yolo_bb);
                if(ch == '\n') {
                    lines++;
                }
            }
            fclose(yolo_bb);

            double yolo_coords[MAX(lines,nboxes)][5];
            box original[lines];
            
            // read file line by line
            yolo_bb = fopen(input, "r");
            if (yolo_bb == NULL)
                exit(1);

            char * line = NULL;
            size_t len = 0;
            ssize_t read;
            char *pch;

            // retreive validation bounding-boxes
            int i = 0;
            while ((read = getline(&line, &len, yolo_bb)) != -1) {
                pch = strtok (line, " ");
                int j = 0;
                while (pch != NULL) {
                    yolo_coords[i][j++] = atof(pch);
                    pch = strtok (NULL, " ");
                }
                original[i].x = yolo_coords[i][1];
                original[i].y = yolo_coords[i][2];
                original[i].w = yolo_coords[i][3];
                original[i].h = yolo_coords[i][4];

                i++;
            }

            fclose(yolo_bb);
            if (line)
                free(line);

            // sort b-boxes by y and x axes
            qsort(original, lines, sizeof(*original), md_comparator);

            // for each validation b-box
            for (int i = 0; i < lines; ++i) {
                // calculate position
                int left  = (original[i].x - original[i].w / 2.) * im.w;
                int right = (original[i].x + original[i].w / 2.) * im.w;
                int top   = (original[i].y - original[i].h / 2.) * im.h;
                int bot   = (original[i].y + original[i].h / 2.) * im.h;
                double width = im.h * .004;

                if(left < 0) left = 0;
                if(right > im.w-1) right = im.w-1;
                if(top < 0) top = 0;
                if(bot > im.h-1) bot = im.h-1;    

                // non max suppression
                if (nms) do_nms_sort(dets, nboxes, l.classes, nms);

                // find for him from detected b-boxes the one with best IoU value
                double best_iou = 0.0;
                float best_iou_prob = -1.0;
                for (int j = 0; j < nboxes; ++j) {
                    double iou = box_iou(original[i], dets[j].bbox);
                    if (iou >= 0.5)
                        dets[j].hit = 1;    // successful detection = green color of b-box
                    else 
                        if(dets[j].hit != 1) dets[j].hit = 0;   // unsuccessful detection = red color of b-box
                    
                    if((iou >= best_iou) && (best_iou_prob == -1.0 || dets[j].prob[0] > thresh)) {
                        best_iou = iou;
                        best_iou_prob = dets[j].prob[0]*100;
                    }
                }

                float rgb[3];
                if (best_iou < 0.5) {
                    // set red color
                    rgb[0] = 1.0;
                    rgb[1] = 0.0;
                    rgb[2] = 0.0;
                } else {
                    // set green color
                    rgb[0] = 0.0;
                    rgb[1] = 1.0;
                    rgb[2] = 0.0;
                }

                draw_box_width(im, left, top, right, bot, width, rgb[0], rgb[1], rgb[2]);
                
                char iou_label[15];
                sprintf(buff, "IoU: %.4f", best_iou);
                strcpy(iou_label, buff);
                
                if ((!test_iou && !probability) || test_iou)
                printf("%s    probability: %.0f%%\n", iou_label, best_iou_prob);    

                if (alphabet) {
                    image label = get_label(alphabet, iou_label, (im.h*.03));
                    draw_label(im, bot + label.h, left, label, rgb);
                    free_image(label);
                }
            }
        } else {
            if (nms) do_nms_sort(dets, nboxes, l.classes, nms);
        }

        if ((!test_iou && !probability) || probability)
            draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes, 1);
        else 
            draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes, 0);
        
        free_detections(dets, nboxes);
        if(!(outfile && !outfile[0])) {
            save_image(im, outfile);
        } else if(!test_iou && !probability) { 
            save_image(im, "predictions");
#ifdef OPENCV
            if (im.w < 1280 || im.h < 720)
                make_window("predictions", im.w, im.h, 0);
            else
                make_window("predictions", 1280, 720, 0);
            show_image(im, "predictions", 0);
#endif
        }

        free_image(im);
        free_image(sized);
        if (filename) break;
    }
}

/***
* Main function for parsing arguments from command line
*
* This function is all modified and expanded
* Added printing of help, calling of new subroutines
* Changed some default values
*
* @param argc Number of arguments
* @param argv List of arguments
*/
void run_detector(int argc, char **argv)
{
    char *prefix = "";
    if (0==strcmp(argv[2], "export"))
        prefix = find_char_arg(argc, argv, "-prefix", "");
    else
        prefix = find_char_arg(argc, argv, "-prefix", "results");
    float thresh = find_float_arg(argc, argv, "-thresh", .5);
    float hier_thresh = find_float_arg(argc, argv, "-hier", .5);
    int cam_index = find_int_arg(argc, argv, "-c", 0);
    int frame_skip = find_int_arg(argc, argv, "-skip", 1);
    int avg = find_int_arg(argc, argv, "-avg", 3);
    if(argc < 3){
        fprintf(stderr, "\nusage: %s %s [train/test/valid]\n More info will follow with function name and -help option\n", argv[0], argv[1]);
        return;
    }
    char *gpu_list = find_char_arg(argc, argv, "-gpus", 0);
    char *outfile = find_char_arg(argc, argv, "-out", "");
    int *gpus = 0;
    int gpu = 0;
    int ngpus = 0;
    if(gpu_list){
        printf("%s\n", gpu_list);
        int len = strlen(gpu_list);
        ngpus = 1;
        int i;
        for(i = 0; i < len; ++i){
            if (gpu_list[i] == ',') ++ngpus;
        }
        gpus = calloc(ngpus, sizeof(int));
        for(i = 0; i < ngpus; ++i){
            gpus[i] = atoi(gpu_list);
            gpu_list = strchr(gpu_list, ',')+1;
        }
    } else {
        gpu = gpu_index;
        gpus = &gpu;
        ngpus = 1;
    }

    int clear = find_arg(argc, argv, "-clear");
    int fullscreen = find_arg(argc, argv, "-fullscreen");
    int width = find_int_arg(argc, argv, "-width", 0);
    int height = find_int_arg(argc, argv, "-height", 0);
    int fps = find_int_arg(argc, argv, "-fps", 0);

    int test_iou = find_arg(argc, argv, "-iou");
    int probability = find_arg(argc, argv, "-probability");

    int export_image = find_arg(argc, argv, "-export_image");
    int export_video = find_arg(argc, argv, "-export_video");
    int show = find_arg(argc, argv, "-show");
    int save_original = find_arg(argc, argv, "-save_original");

    char *datacfg = find_char_arg(argc, argv, "-data", "data/data/universal.data");
    char *cfg = find_char_arg(argc, argv, "-cfg", "");
    char *weights = find_char_arg(argc, argv, "-weights", "");
    char *filename = find_char_arg(argc, argv, "-filename", 0);
    int help = find_arg(argc, argv, "-h");

    char *weights_file_name = find_char_arg(argc, argv, "-weights_name", "");
    char *graph_dir = find_char_arg(argc, argv, "-graph_dir", "");
    char *train_file = find_char_arg(argc, argv, "-train", "");
    char *backup_dir = find_char_arg(argc, argv, "-backup", "");

    char *valid_images = find_char_arg(argc, argv, "-valid", "");

    if(0==strcmp(argv[2], "test")) {
        if ((help) || (cfg && !cfg[0]) || (weights && !weights[0])) {
            fprintf(stderr, "\nusage: %s %s %s [-data DATA_FILE (default cfg/data/universal.data)] -cfg CFG_FILE -weights WEIGHT_FILE [-iou] [-probability] [-thresh THRESH] [-filename IMAGE]\n", argv[0], argv[1], argv[2]);
            return;
        }
        test_detector(datacfg, cfg, weights, filename, thresh, hier_thresh, outfile, fullscreen, test_iou, probability);
    
    } else if(0==strcmp(argv[2], "train")) {
        if ((help) || (weights_file_name && !weights_file_name[0]) || (cfg && !cfg[0]) || (weights && !weights[0]) || (graph_dir && !graph_dir[0]) || (train_file && !train_file[0]) || (backup_dir && !backup_dir[0])) {
            fprintf(stderr, "\nusage: %s %s %s -weights_name WEIGHTS_NAME -cfg CFG_FILE -weights WEIGHT_FILE -graph_dir GRAPH_DIRECTORY -train TRAIN_FILE -backup BACKUP_DIR\n", argv[0], argv[1], argv[2]);
            return;
        }

        //added creation of file with loss function value

        // get current time
        time_t mytime = time(NULL);
        char * time_str = ctime(&mytime);
        time_str[strlen(time_str)-1] = '\0';
        
        // change ' ' to '_'  
        int i=0;
        char cur_time[30];
        strcpy(cur_time, time_str);
        while (cur_time[i]) {
            if (isspace(cur_time[i])) 
                cur_time[i]='_';
            i++;
        }

        char graph_name[100];
        strcpy(graph_name, graph_dir);
        strcat(graph_name, weights_file_name);
        strcat(graph_name, "_");    
        strcat(graph_name, cur_time);
        strcat(graph_name, "_graph.txt");
        
        remove(graph_name);
        printf("%s\n", graph_name);

        train_detector(weights_file_name, cfg, weights, gpus, ngpus, clear, graph_name, train_file, backup_dir);

    }
    else if(0==strcmp(argv[2], "valid")) {
        if ((help) || (cfg && !cfg[0]) || (weights && !weights[0]) || (outfile && !outfile[0])) {
            fprintf(stderr, "\nusage: %s %s %s [-data DATA_FILE (default cfg/data/universal.data)] -cfg CFG_FILE -weights WEIGHT_FILE [-prefix OUTPUT_DIR (default results/)] -out OUTPUT_FILE_NAME -valid FILE_WITH_VALIDATE_IMAGES\n", argv[0], argv[1], argv[2]);
            return;
        }
        validate_detector(datacfg, cfg, weights, prefix, outfile, valid_images);
    }
    else if(0==strcmp(argv[2], "valid2")) validate_detector_flip(datacfg, cfg, weights, outfile);
    else if(0==strcmp(argv[2], "recall")) validate_detector_recall(cfg, weights);
    else if(0==strcmp(argv[2], "demo")) {
        if ((help) || (cfg && !cfg[0]) || (weights && !weights[0])) {
            fprintf(stderr, "\nusage: %s %s %s [-data DATA_FILE (default cfg/data/universal.data)] -cfg CFG_FILE -weights WEIGHT_FILE [-thresh THRESH] [-filename VIDEO (default webcam)]\n", argv[0], argv[1], argv[2]);
            return;
        }
        list *options = read_data_cfg(datacfg);
        int classes = option_find_int(options, "classes", 20);
        char *name_list = option_find_str(options, "names", "data/names.list");
        char **names = get_labels(name_list);
        demo(cfg, weights, thresh, cam_index, filename, names, classes, 1, 0, avg, hier_thresh, width, height, fps, fullscreen, 0, 0, 1, 0, 0);
    } else if(0==strcmp(argv[2], "export")) {
        if ((help) || (cfg && !cfg[0]) || (weights && !weights[0]) || (prefix && !prefix[0])) {
            fprintf(stderr, "\nusage: %s %s %s [-data DATA_FILE (default cfg/data/universal.data)] -cfg CFG_FILE -weights WEIGHT_FILE -prefix OUTPUT_DIR [-thresh THRESH] [-filename VIDEO (default webcam)] [-export_image] [-export_video] [-show] [-fps FPS (default: video default)]\n", argv[0], argv[1], argv[2]);
            fprintf(stderr, "-prefix must be full path to OUTPUT_DIR, also after OUTPUT_DIR can be OUTPUT_FILE_NAME prefix\n");
            return;
        } 
        if(!export_video && !export_image) {
            fprintf(stderr, "Select at least one of -export arguments\n");
            exit(1);
        }
        list *options = read_data_cfg(datacfg);
        int classes = option_find_int(options, "classes", 20);
        char *name_list = option_find_str(options, "names", "data/names.list");
        char **names = get_labels(name_list);
        demo(cfg, weights, thresh, cam_index, filename, names, classes, 1, prefix, avg, hier_thresh, width, height, fps, fullscreen, export_image, export_video, show, 0, 0);
    } else if(0==strcmp(argv[2], "save_detections")) {
        if ((help) || (cfg && !cfg[0]) || (weights && !weights[0]) || (prefix && !prefix[0])) {
            fprintf(stderr, "\nusage: %s %s %s [-data DATA_FILE (default cfg/data/universal.data)] -cfg CFG_FILE -weights WEIGHT_FILE -prefix OUTPUT_DIR [-thresh THRESH] [-filename VIDEO (default webcam)] [-show] [-fps FPS (default: video default)] [-skip FRAME_SKIP (default 1)] [-save_original]\n", argv[0], argv[1], argv[2]);
            fprintf(stderr, "-prefix must be full path to OUTPUT_DIR, also after OUTPUT_DIR can be OUTPUT_FILE_NAME prefix\n");
            return;
        }
        list *options = read_data_cfg(datacfg);
        int classes = option_find_int(options, "classes", 20);
        char *name_list = option_find_str(options, "names", "data/names.list");
        char **names = get_labels(name_list);
        demo(cfg, weights, thresh, cam_index, filename, names, classes, frame_skip, prefix, avg, hier_thresh, width, height, fps, fullscreen, export_image, export_video, show, 1, save_original);
    } else {
        fprintf(stderr, "Wrong function\n");
        exit(1);
    }        
}
