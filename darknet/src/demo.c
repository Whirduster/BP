/**********************************************************************
*
*  Taken and modified code
*  
*  Original author: Joseph Redmon
*  Author of modifications: Tomas Libal
*
*  Modified parts of functions are marked by comments
*  Some parts of the code were removed because they were unnecessary
*
*  Modified functions: 
*       detect_in_thread()
*       fetch_in_thread()
*       display_in_thread()
*       demo()
*
*  Added functions:
*        create_name_and_save_image()
*        create_name_and_save_bboxes()
*
**********************************************************************/

#include "network.h"
#include "detection_layer.h"
#include "region_layer.h"
#include "cost_layer.h"
#include "utils.h"
#include "parser.h"
#include "box.h"
#include "image.h"
#include "demo.h"
#include <sys/time.h>

#define DEMO 1

#ifdef OPENCV

static char **demo_names;
static image **demo_alphabet;
static int demo_classes;

static network *net;
static image buff [3];
static image buff_letter[3];
static image original;      // added
static image last_showed;   // added
static int buff_index = 0;
static void * cap;
static void * output_video;
static float fps = 0;
static float demo_thresh = 0;
static float demo_hier = .5;
static int running = 0;

static int demo_frame = 3;
static int demo_index = 0;
static float **predictions;
static float *avg;
static int demo_done = 0;
static int demo_total = 0;
double demo_time;

// added
static detection *dets;
static int nboxes = 0;

detection *get_network_boxes(network *net, int w, int h, float thresh, float hier, int *map, int relative, int *num);

int size_network(network *net)
{
    int i;
    int count = 0;
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            count += l.outputs;
        }
    }
    return count;
}

void remember_network(network *net)
{
    int i;
    int count = 0;
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            memcpy(predictions[demo_index] + count, net->layers[i].output, sizeof(float) * l.outputs);
            count += l.outputs;
        }
    }
}

detection *avg_predictions(network *net, int *nboxes)
{
    int i, j;
    int count = 0;
    fill_cpu(demo_total, 0, avg, 1);
    for(j = 0; j < demo_frame; ++j){
        axpy_cpu(demo_total, 1./demo_frame, predictions[j], 1, avg, 1);
    }
    for(i = 0; i < net->n; ++i){
        layer l = net->layers[i];
        if(l.type == YOLO || l.type == REGION || l.type == DETECTION){
            memcpy(l.output, avg + count, sizeof(float) * l.outputs);
            count += l.outputs;
        }
    }
    detection *dets = get_network_boxes(net, buff[0].w, buff[0].h, demo_thresh, demo_hier, 0, 1, nboxes);
    return dets;
}


/***
 * Modified function for detecting object in video frame
 *
 * NMS increased to 0.45, some structures are now global
*/
void *detect_in_thread(void *ptr)
{
    running = 1;
    float nms = .45;

    layer l = net->layers[net->n-1];
    float *X = buff_letter[(buff_index+2)%3].data;
    network_predict(net, X);

    remember_network(net);
    dets = avg_predictions(net, &nboxes);

    if (nms) do_nms_sort(dets, nboxes, l.classes, nms);   // nonmaximum supression and sorting

    printf("\033[2J");
    printf("\033[1;1H");
    printf("\nFPS:%.1f\n",fps);
    printf("Objects:\n\n");
    image display = buff[(buff_index+2) % 3];
    draw_detections(display, dets, nboxes, demo_thresh, demo_names, demo_alphabet, demo_classes, 1);
    //free_detections(dets, nboxes);

    demo_index = (demo_index + 1)%demo_frame;
    running = 0;
    return 0;
}

/***
 * Modified function for getting frame from video
 *
 * Added image saving for later use
*/
void *fetch_in_thread(void *ptr)
{
    free_image(buff[buff_index]);
    buff[buff_index] = get_image_from_stream(cap);
    original = copy_image(buff[buff_index]);    //image saving  for later use
    if(buff[buff_index].data == 0) {
        demo_done = 1;
        return 0;
    }
    letterbox_image_into(buff[buff_index], net->w, net->h, buff_letter[buff_index]);
    return 0;
}

/***
 * Modified function for getting frame from video
 *
 * Added image saving for later use
 *
 * @param show If is true, displays the current image, otherwise the last showed image will be displayed again
*/
void *display_in_thread(void *ptr, int show)
{
    int c;
    if(show)
        c = show_image(buff[(buff_index + 1)%3], "Demo", 1);  // show new image
    else
        c = show_image(last_showed, "Demo", 1); // show again last image

    if (c != -1) c = c%256;
    if (c == 27) {
        demo_done = 1;
        return 0;
    } else if (c == 82) {
        demo_thresh += .02;
    } else if (c == 84) {
        demo_thresh -= .02;
        if(demo_thresh <= .02) demo_thresh = .02;
    } else if (c == 83) {
        demo_hier += .02;
    } else if (c == 81) {
        demo_hier -= .02;
        if(demo_hier <= .0) demo_hier = .0;
    }
    return 0;
}

void *display_loop(void *ptr)
{
    while(1){
        display_in_thread(0, 0);
    }
}

void *detect_loop(void *ptr)
{
    while(1){
        detect_in_thread(0);
    }
}

/***
 * Function for creating image in given directory with given prefix
 *
 * @param image_to_save Image to be saved
 * @param prefix Path to directory in which will be image saved and image name prefix
 * @param count Frame number which will be appended to name of output image
*/
void create_name_and_save_image(image image_to_save, char *prefix, int count) {
    char name[256];
    sprintf(name, "%s_%08d", prefix, count);
    save_image(image_to_save, name);
}

/***
 * Function for creating txt file with image bounding-boxes in given directory with given prefix
 *
 * @param prefix Path to directory in which will be image saved and image name prefix
 * @param count Frame number which will be appended to name of output image
*/
void create_name_and_save_bboxes(char *prefix, int count) {
    char name[256];
    sprintf(name, "%s_%08d.txt", prefix, count);
    FILE *file = fopen(name, "w");
    if (file == NULL) {
        fprintf(stderr, "Error opening file!\n");
        exit(1);
    }
    for (int i = 0; i < nboxes; ++i) {
        if(dets[i].prob[0] > demo_thresh) {
            printf("0 %f %f %f %f\n", dets[i].bbox.x, dets[i].bbox.y, dets[i].bbox.w, dets[i].bbox.h);  
            fprintf(file, "0 %f %f %f %f\n", dets[i].bbox.x, dets[i].bbox.y, dets[i].bbox.w, dets[i].bbox.h);
        }
    }
    free_detections(dets, nboxes);
    fclose(file);
}

/***
 * Modified function for running detections in video file
 * 
 * This function is all modified and expanded
 * Delay was firstly unused, now it determines, how many frames will be skipped
 * Added functions for saving video frames as images
 *
 * @param cfgfile Path to configuration file
 * @param weightfile Path to .weights file
 * @param thresh Lower treshold
 * @param cam_index Camera index (if multiple cameras are connected)
 * @param filename Path to input video file
 * @param names List of names of object classes 
 * @param classes Number of object classes
 * @param delay Number of frames to be skipped
 * @param prefix Path to directory in which will be image saved and image name prefix
 * @param avg_frames Unused - leave it as it is
 * @param hier Not sure what is this doing - leave default value .5 as it is
 * @param w Width of final video
 * @param h Height of final video
 * @param frames Frame rate of final video
 * @param fullscreen If is true, video is rendered to fullscreen mode
 * @param export_image If is true, each frame will be saved individually as image
 * @param export_video If is true, final video with marked detections will be saved
 * @param show If is true, render the video into a window on the screen
 * @param save_detections If is true, only frames with detected licence plates will be saved
 * @param save_original If is true, only frames with detected licence plates will be saved, but the bounding box in the image will not be rendered
*/
void demo(char *cfgfile, char *weightfile, float thresh, int cam_index, const char *filename, char **names, int classes, int delay, char *prefix, int avg_frames, float hier, int w, int h, int frames, int fullscreen, int export_image, int export_video, int show, int save_detections, int save_original)
{
    //demo_frame = avg_frames;
    image **alphabet = load_alphabet();
    demo_names = names;
    demo_alphabet = alphabet;
    demo_classes = classes;
    demo_thresh = thresh;
    demo_hier = hier;
    printf("Demo\n");
    net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 1);
    pthread_t detect_thread;
    pthread_t fetch_thread;

    srand(2222222);

    int i;
    demo_total = size_network(net);
    predictions = calloc(demo_frame, sizeof(float*));
    for (i = 0; i < demo_frame; ++i){
        predictions[i] = calloc(demo_total, sizeof(float));
    }
    avg = calloc(demo_total, sizeof(float));

    if(filename){
        printf("video file: %s\n", filename);
        cap = open_video_stream(filename, 0, 0, 0, 0);
    }else{
        cap = open_video_stream(0, cam_index, w, h, frames);
    }

    if(!cap) error("Couldn't connect to webcam.\n");

    w = get_frame_width(cap);
    h = get_frame_height(cap);
    if(export_video) {
        if(!frames)
            frames = get_video_fps(cap);
        char video_name[256];
        strcpy(video_name, prefix);
        strcat(video_name, ".avi");
        output_video = open_output_video_stream(video_name, w, h, frames);
    }

    buff[0] = get_image_from_stream(cap);
    buff[1] = copy_image(buff[0]);
    buff[2] = copy_image(buff[0]);
    original = copy_image(buff[buff_index]);
    buff_letter[0] = letterbox_image(buff[0], net->w, net->h);
    buff_letter[1] = letterbox_image(buff[0], net->w, net->h);
    buff_letter[2] = letterbox_image(buff[0], net->w, net->h);

    int count = 0;
    if(show){
        make_window("Demo", w, h, fullscreen);
    }

    last_showed = make_empty_image(w, h, 0);

    demo_time = what_time_is_it_now();

    while(!demo_done){
        buff_index = (buff_index + 1) %3;

        if(save_detections && ((count % delay) == 0) && save_original && nboxes > 0)
            create_name_and_save_image(original, prefix, count);
        
        if(pthread_create(&fetch_thread, 0, fetch_in_thread, 0)) error("Thread creation failed");
        if(pthread_create(&detect_thread, 0, detect_in_thread, 0)) error("Thread creation failed");
        if(show){
            fps = 1./(what_time_is_it_now() - demo_time);
            demo_time = what_time_is_it_now();
            if(save_detections) {
                if ((count % delay) == 0) {
                    if(nboxes > 0)
                        last_showed = copy_image(buff[(buff_index + 1)%3]);
                }
                display_in_thread(0, 0);
            } else {
                display_in_thread(0, 1);
            }
        }
        if(save_detections && ((count % delay) == 0) && !save_original && nboxes > 0)
            create_name_and_save_image(buff[(buff_index + 1)%3], prefix, count);

        if(export_image)
            create_name_and_save_image(buff[(buff_index + 1)%3], prefix, count);

        if(export_video)
            write_image_to_video(output_video, buff[(buff_index + 1)%3]);
        
        if(save_detections && ((count % delay) == 0) && nboxes > 0)
            create_name_and_save_bboxes(prefix, count);

        pthread_join(fetch_thread, 0);
        pthread_join(detect_thread, 0);
        ++count;
    }
}
#else
void demo(char *cfgfile, char *weightfile, float thresh, int cam_index, const char *filename, char **names, int classes, int delay, char *prefix, int avg, float hier, int w, int h, int frames, int fullscreen)
{
    fprintf(stderr, "Demo needs OpenCV for webcam images.\n");
}
#endif

